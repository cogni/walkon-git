Hi!

In order to activate your account please visit the following link:

http://{{ site }}{% url register_activate activation_key=activation_key %}

Thanks for joining,
Support team
{{ site }}