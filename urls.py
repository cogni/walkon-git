# -*- coding: utf-8 -*-
from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template
from userprofile.forms import PasswordResetForm

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('userprofile.views',
    url(r'^account/img/(?P<userkey>[.a-zA-Z0-9_\-]+)/(?P<filename>.*)$', 'avatar_image', name='avatar_image'),
    url(r'^account/conf/$', 'user_settings', name='user_settings'),
    url(r'^account/avatar/$', 'user_avatar', name='user_avatar'),
    url(r'^account/theme/$', 'user_theme', name='user_theme'),
    url(r'^account/bg/(?P<userkey>[.a-zA-Z0-9_\-]+)/(?P<filename>.*)$', 'theme_image', name='theme_image'),
    url(r'^account/links/$', 'user_link', name='user_link'),
    url(r'^account/link/(?P<link_key>[.a-zA-Z0-9_\-]+)/remove/$', 'link_remove', name='user_linkremove'),
    url(r'^account/admin/$', 'user_admin', name='user_admin'),
    url(r'^account/admin/(?P<username>[.a-zA-Z0-9_@\-]+)/$', 'user_admin_detail', name='user_admin_detail'),
)

urlpatterns += patterns('',
    url(r'^account/login/$', 'userprofile.views.email_login', {'template_name': 'userprofile/login.html'}, name='login'),
    url(r'^account/logout/$', 'userprofile.views.do_logout', name='logout'),
    url(r'^account/register/$', 'userprofile.views.register_inactive_user', name='register'),
    url(r'^account/register/complete/$', direct_to_template, {'template': 'userprofile/register_complete.html'}, name='register_complete'),
    url(r'^account/activate/complete/$', 'userprofile.views.activate_complete', name='register_activate_complete'),
    url(r'^account/activate/(?P<activation_key>\w+)/$', 'userprofile.views.activate', name='register_activate'),
    url(r'^account/password/change/$', 'django.contrib.auth.views.password_change', 
        {'template_name':'userprofile/password_change_form.html'}, name='user_pass_change'),
    url(r'^account/password/change/complete/$', 'django.contrib.auth.views.password_change_done', 
        {'template_name':'userprofile/password_change_done.html'}, name='user_pass_change_done'),
    url(r'^account/recover/$', 'django.contrib.auth.views.password_reset', 
            {'template_name':'userprofile/getpassword.html', 'email_template_name':'userprofile/password_reset_email.html',
             'password_reset_form':PasswordResetForm}, name='password_reset'),
    url(r'^account/recover/complete/$', 'django.contrib.auth.views.password_reset_done', 
            {'template_name': 'userprofile/password_reset_done.html'}, name='reset_done'),
    url(r'^account/confirm/(?P<uidb36>.+)/(?P<token>.+)/$', 'django.contrib.auth.views.password_reset_confirm', 
            {'template_name': 'userprofile/password_reset_confirm.html'}, name='password_reset_confirm'),
    url(r'^account/confirm/complete/$', 'django.contrib.auth.views.password_reset_complete', 
            {'template_name': 'userprofile/password_reset_complete.html'}, name='password_reset_last'),
)

urlpatterns += patterns('walkon.views',
    url(r'^on/(?P<username>[.a-zA-Z0-9_@\-]+)/$','userpage', name='userpage'),
    url(r'^cfg/$','global_config', name='cfg_main'),
    url(r'^cfg/goal/$','goal', name='cfg_goal'),
    url(r'^cfg/activities/$','activities', name='cfg_activities'),
)

#Initializations 

urlpatterns += patterns('walkon.views',
    url(r'^getsteptable/$', 'get_step_table', name='step_table_ajax'),
    url(r'^getfullstepform/$', 'get_full_step_form', name='full_step_form_ajax'),
    url(r'^reports/$', 'reports', name='reports'),
    url(r'^reports/fullresults/$', 'full_results', name='full_results'),
    url(r'^reports/map/$', 'reports', name='report_map'),
    url(r'^reports/activities/$', 'reports_activities', name='report_activities'),
    url(r'^reports/benchmark/$', 'reports_benchmark', name='report_benchmark'),
    url(r'^reports/projections/$', 'reports_projections', name='report_projections'),
    url(r'^reports/rankings/$', 'reports_rankings', name='report_rankings'),
    url(r'^reports/steps/$', 'reports_steps', name='report_steps'),
    url(r'^bg/(?P<filename>.*)$', 'global_theme_image', name='global_theme_image'),
    (r'^updatestats$', 'update_user_stats'),
    (r'^cropstats$', 'crop_stats'),
    (r'^clearstats$', 'clear_stats'),
    (r'^clearsteps$', 'clear_steps'),
    (r'^fillconfig$', 'create_config'),
    (r'^init$', 'initialization'),    
    url(r'^$', 'index', name="index"),
    (r'^admin/(.*)', admin.site.root),
)

