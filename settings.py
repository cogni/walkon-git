# -*- coding: utf-8 -*-
from ragendja.settings_pre import *
import os

ADMINS = (
    ('', ''),
)

MANAGERS = ADMINS

# Increase this when you update your media on the production site, so users
# don't have to refresh their cache. By setting this your MEDIA_URL
# automatically becomes /media/MEDIA_VERSION/
MEDIA_VERSION = 1

# Add base media (jquery can be easily added via INSTALLED_APPS)
#COMBINE_MEDIA = {
    #'combined-%(LANGUAGE_CODE)s.js': (
        ## See documentation why site_data can be useful:
        ## http://code.google.com/p/app-engine-patch/wiki/MediaGenerator
        #'.site_data.js',
    #),
    #'combined-%(LANGUAGE_DIR)s.css': (
        ##'global/look.css',
    #),
#}


# Make this unique, and don't share it with anybody.
SECRET_KEY = 'ih4fth85hc5&%$V%$Y&534Y6%'

#ENABLE_PROFILER = True
#ONLY_FORCED_PROFILE = True
##PROFILE_PERCENTAGE = 25
#SORT_PROFILE_RESULTS_BY = 'cumulative' # default is 'time'
##PROFILE_PATTERN = 'ext.db..+\((?:get|get_by_key_name|fetch|count|put)\)'

# Enable I18N and set default language to 'en'
USE_I18N = True
LANGUAGE_CODE = 'en'

#Restrict supported languages (and JS media generation)
LANGUAGES = (
#    ('de', 'German'),
    ('en', 'English'),
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.auth',
    'django.core.context_processors.media',
    'django.core.context_processors.request',
    'django.core.context_processors.i18n',
    'walkon.context_processors.goal'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    # Django authentication
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    # Google authentication
    #'ragendja.auth.middleware.GoogleAuthenticationMiddleware',
    # Hybrid Django/Google authentication
    #'ragendja.auth.middleware.HybridAuthenticationMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'ragendja.sites.dynamicsite.DynamicSiteIDMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
    'django.contrib.redirects.middleware.RedirectFallbackMiddleware',
)

AUTHENTICATION_BACKENDS = (
    'userprofile.email_auth.EmailBackend',
)

# Google authentication
#AUTH_USER_MODULE = 'ragendja.auth.google_models'
#AUTH_ADMIN_MODULE = 'ragendja.auth.google_admin'
# Hybrid Django/Google authentication
#AUTH_USER_MODULE = 'ragendja.auth.hybrid_models'

GLOBALTAGS = (
    'ragendja.templatetags.ragendjatags',
    'django.templatetags.i18n',
)

TEMPLATE_DIRS = (
    os.path.join(os.path.dirname(__file__), 'templates'),
)

TEMPLATE_LOADERS = (
    'dbtemplates.loader.load_template_source',
    'django.template.loaders.filesystem.load_template_source',
    'django.template.loaders.app_directories.load_template_source',
#     'django.template.loaders.eggs.load_template_source',
)

LOGIN_URL = '/account/login/'
LOGOUT_URL = '/account/logout/'
LOGIN_REDIRECT_URL = '/'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.sessions',
    'django.contrib.admin',
    'django.contrib.webdesign',
#    'django.contrib.flatpages',
    'django.contrib.redirects',
#    'django.contrib.sites',
    'appenginepatcher',
    'dbtemplates',
    'cogni',
    'userprofile',
    'walkon',
    'mediautils',
)

# List apps which should be left out from app settings and urlsauto loading
IGNORE_APP_SETTINGS = IGNORE_APP_URLSAUTO = (
    # Example:
    # 'django.contrib.admin',
    # 'django.contrib.auth',
    # 'yetanotherapp',
)

# Remote access to production server (e.g., via manage.py shell --remote)
DATABASE_OPTIONS = {
    # Override remoteapi handler's path (default: '/remote_api').
    # This is a good idea, so you make it not too easy for hackers. ;)
    # Don't forget to also update your app.yaml!
    #'remote_url': '/remote-secret-url',

    # !!!Normally, the following settings should not be used!!!

    # Always use remoteapi (no need to add manage.py --remote option)
    #'use_remote': True,

    # Change appid for remote connection (by default it's the same as in your app.yaml)
    #'remote_id': 'otherappid',

    # Change domain (default: <remoteid>.appspot.com)
    #'remote_host': 'bla.com',
}

AUTH_USER_MODULE = 'userprofile.models'

ACCOUNT_ACTIVATION_DAYS = 3

#design default values
BG_COLOR = '#E5ECF9'
MAX_UPLOAD_SIZE = 307200

DJANGO_STYLE_MODEL_KIND = False

DOMAIN_URL = "http://cognitiva-walkon.appspot.com"

from ragendja.settings_post import *
