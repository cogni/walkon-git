from applib import urlnorm
import urlparse

def add_slash(url):
    if url[-1] != '/':
        return url + '/'
    else:
        return url
        
def url_normalizated(iurl):
    '''url normalization rules from http://en.wikipedia.org/wiki/URL_normalization'''
    #Converting the scheme and host to lower case
    #Removing dot-segments
    #Removing the default port
    #Converting the entire URL to lower case
    
    if not iurl:
        return iurl
    try:
        url = urlnorm.norms(iurl)
    except:
        #bad links get empty return
        return ''
    #Removing www as the first domain label
    if len(url) > 11 and url[:11] == 'http://www.':
        url = 'http://' + url[11:]
    if len(url) > 12 and url[:12] == 'https://www.':
        url = 'https://' + url[12:]
    #Adding trailing /
    url_parsed = urlparse.urlparse(url)
    path = url_parsed[2]
    path_elements = path.split('/')
    if path == '':
        url = add_slash(url)
    elif len(path_elements) > 0 and path_elements[-1].find('.') == -1:
        url = add_slash(url)
    #Removing directory index if there is no query string and no parameters
    if url_parsed[3] == '' and  url_parsed[4] == '' and len(path_elements) > 0:
        if path_elements[-1] in ('index.html','index.htm','index.php','default.asp'):
            url = url[:-len(path_elements[-1])]
    #Removing the "?" when the querystring is empty
    if url[-1] == '?':
        url = url[:-1]
    return url
    
def test_urlnorm():
    tests = {    
        'http://foo.com':                    'http://foo.com/',
        'http://www.foo.com':                'http://foo.com/',
        'http://www.foo.com/':               'http://foo.com/',
        'http://www.foo.com/?':              'http://foo.com/',
        'http://www.foo.com/bar/index.html': 'http://foo.com/bar/',
        'http://foo.com/bar/page.html':      'http://foo.com/bar/page.html',
        'http://foo.com/bar/page.html?':     'http://foo.com/bar/page.html',
        'http://foo.com/bar/page.html?a=1':  'http://foo.com/bar/page.html?a=1',
        'http://www.jornaldenegocios.pt/default.asp?Session=&CpContentId=308507': 'http://jornaldenegocios.pt/default.asp?Session=&CpContentId=308507'
    }
    n_correct, n_fail = 0, 0
    test_keys = tests.keys()
    test_keys.sort()            
    for i in test_keys:
        print i
        cleaned = url_normalizated(i)
        print cleaned
        answer = tests[i]
        if cleaned != answer:
            print "*** TEST FAILED: " + i
            n_fail = n_fail + 1
        else:
            n_correct = n_correct + 1        
    print "TOTAL CORRECT:", n_correct
    print "TOTAL FAILURE:", n_fail