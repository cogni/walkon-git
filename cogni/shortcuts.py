from google.appengine.ext import db
from django.http import Http404
from django.core.exceptions import MultipleObjectsReturned

def get_object_or_404(klass, *args):
    """
    Return a single object of the class and filter conditions, or 
    raise an exception. Allows several filter arguments in sequence. 
    Example: 
    entry = get_object_or_404(Entry, 'user =', user, 'created_on =', today)
    """
    queryset = klass.all()
    #each pair in args becomes a filter  
    filter_args = [ (args[i], args[i+1]) for i in range(0, len(args)-1, 2) ]
    for f_args in filter_args:
        queryset = queryset.filter(*f_args)
    results = queryset.fetch(2)
    if len(results) == 0:
        raise Http404('No record matches the given query.')
    if len(results) > 1:
        raise MultipleObjectsReturned("More than one record returned")
    else:
        return results[0]


def getkey_object_or_404(klass, key):
    """
    Return a single object of the class from the key, or 
    raise an exception. 
    """
    obj = klass.get(db.Key(key))
    if obj is None:
        raise Http404('No record matches the given query.')
    else:
        return obj
