from google.appengine.ext import db
from userprofile.models import User, Theme

class Goal(db.Model):
    """Objective

    Attributes:
        start - name of the starting location
        start_geo - geolocation of the starting point
        finish - name of the destination
        finish_geo - geolocation of the finishing point
        start_date - date
        end_date - date
        total_distance - in km
        directions - encoded polyline string for google maps containing the path to travel
    """
    start = db.StringProperty(required=True)
    start_geo = db.GeoPtProperty()
    finish = db.StringProperty(required=True)
    finish_geo = db.GeoPtProperty()
    start_date = db.DateProperty()
    end_date = db.DateProperty()
    total_distance = db.FloatProperty()
    directions = db.TextProperty()

    def jsdirections(self):
        """Returns a string ready to be inserted in javascript code - all backslashes escaped as double backslashes """
        return str(self.directions).replace('\\','\\\\')


class Activity(db.Model):
    """Conversion of minutes spent in a Activity to steps

    Attributes:
        name - name of the activity, to be displayed to the enduser -> = to the key_name!
        steps_min - number of steps/min on the activity
        default - one of the default activities to show in the distance input form
    """
    name = db.StringProperty(required=True)
    steps_min = db.IntegerProperty(required=True)
    default = db.BooleanProperty()

class Step(db.Model):
    """Distance covered by an activity
        Attributes:
            distance - distance in km
            date - date of the activity
            activity - FK
            user - reference to the User object
    """
    user = db.ReferenceProperty(reference_class=User)
    steps = db.IntegerProperty(required=True)
    distance = db.FloatProperty(required=True)
    date = db.DateProperty(required=True)
    activity = db.ReferenceProperty(reference_class=Activity)

class DailyStats(db.Model):
    """Statistics for a user on a given date.
    
    Necessary because you can only fecth 1000 rows in a query, and that can make it impossible to calculate
    the reports on the fly from the Step rows.
    
        daily_distance - distance in km covered on this day
        cumulative_distance - total distance covered by the user until this day
        date - day
        user - reference to the User object
    """
    user = db.ReferenceProperty(reference_class=User)
    daily_distance = db.FloatProperty(required=True)
    cumulative_distance = db.FloatProperty(required=True)
    date = db.DateProperty(required=True)

class StatsByActivity(db.Model):
    """Statistics by activity
    
         user - reference to the User object
         distance - distance recorded performing this activity
         activity - FK
    """
    user = db.ReferenceProperty(reference_class=User)
    distance = db.FloatProperty(required=True)
    activity = db.ReferenceProperty(reference_class=Activity)
    
class GlobalStatsByActivity(db.Model):
    """Global Statistics by activity

         distance - distance recorded performing this activity
         activity - FK
    """
    distance = db.FloatProperty(required=True)
    activity = db.ReferenceProperty(reference_class=Activity)

class GlobalConfig(db.Model):
    """Global configurations
        
        maps_key - google maps key, if available
        from_email - email used in the from field of sent emails
        allowed_domains - list of domains accepted for user emails
    """
    maps_key = db.StringProperty()
    from_email = db.EmailProperty()
    allowed_domains = db.TextProperty()
    bg_name = db.StringProperty()
    bg_image = db.BlobProperty(default=None)
    bg_color = db.StringProperty()
    bg_repeat = db.BooleanProperty()
    site_color1 = db.StringProperty()
    site_color2 = db.StringProperty()
    link_color = db.StringProperty()
    theme = db.Reference(Theme)

    def has_theme(self):
        if self.theme or self.bg_name or self.bg_image or self.bg_color:
            return True

    def get_bgimage(self):
        """return the user, theme or default background image"""
        if self.theme:
            return self.theme.bg_image_path
        elif self.bg_name:
            return reverse('global_theme_image', kw_args={'filename':self.bg_name})
        else:
            return None

    def get_bgcolor(self):
        """returns the user background color or the default background color"""
        return self.get_theme_field('bg_color')
    
    def get_bgrepeat(self):
        return self.get_theme_field('bg_repeat')

    def get_linkcolor(self):
        return self.get_theme_field('link_color')

    def get_sitecolor1(self):
        return self.get_theme_field('site_color1')

    def get_sitecolor2(self):
        return self.get_theme_field('site_color2')

    def get_theme_field(self, fieldname):
        if getattr(self, fieldname):
            return getattr(self, fieldname)
        elif self.theme:
            return getattr(self.theme, fieldname)
        else:
            return None


