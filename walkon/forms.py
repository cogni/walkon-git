# -*- coding: utf-8 -*-
from datetime import date
import time
import logging
from google.appengine.ext.db import GeoPt, GqlQuery
from django import forms
from django.utils.translation import ugettext as _
from django.forms.fields import EMPTY_VALUES
import settings
from models import Goal
from walkon.removable_file_field import RemovableFileFormField

class DateForm(forms.Form):
    date = forms.DateField(label=_("Date"), help_text=_("Date format (yyyy-mm-dd)"))
    show_cal = forms.IntegerField(widget=forms.HiddenInput(), required=False, initial=1)

def validate_int(value):
        """
        Validates that int() can be called on the input. Returns the result
        of int(). Returns None for empty values.
        """
        if value in EMPTY_VALUES:
            return None
        try:
            value = int(str(value))
        except (ValueError, TypeError):
            raise forms.ValidationError(_(u'Enter a whole number.'))
        return value

def validate_date(value, required=False):
    """
    Validates that the input can be converted to a date. Returns a Python
    datetime.date object.
    """
    if value in EMPTY_VALUES:
        if not required:
            return None
    elif isinstance(value, date):
        return value
    else:
        try:
            return date(*time.strptime(value, '%Y-%m-%d')[:3])
        except ValueError:
            pass
    raise forms.ValidationError(_(u'Enter a valid date.'))

class StepForm:
    """My own StepForm.
    
    The problem is that this form is so dynamic that we can never be sure of what the state was when it was
    submitted, so I need a general validation that is independent of the current fields.
    """
    def __init__(self, postdata=None, activities=[], initial=None):
        self.errors = []
        self.steps = self.date_hidden = None
        if initial:
            self.validate(initial)
        if postdata:
            self.validate(postdata, posted=True)
        self.activities = [dict(form_id='act' + str(act.key()), label=_(act.name), help_text=act.steps_min) for act in activities]
        
    def validate(self, initial, posted=False):
        """posted = False - validate that data is valid for the type of field; True - global logic validation"""
        # steps
        self.errors = []
        try:
            self.steps = validate_int(initial.get('steps', None))
        except forms.ValidationError, e:
            self.errors.extend(e.messages)
        # date-hidden
        try:
            self.date_hidden = validate_date(initial.get('date_hidden', None), required=True)
            if posted:
                if self.date_hidden > date.today():
                    raise forms.ValidationError(_("Can't input values in a future date."))
                goal = Goal.all().get()
                if self.date_hidden < goal.start_date:
                    raise forms.ValidationError(_("Can't input values before the start date."))
                if self.date_hidden > goal.end_date:
                    raise forms.ValidationError(_("Can't input values after the finish date."))
        except forms.ValidationError, e:
            self.errors.extend(e.messages)

        # activities
        try:
            self.activity_inputs = [ (name[3:], validate_int(value)) for name, value in initial.items() if value not in EMPTY_VALUES and name.startswith('act') ]
        except forms.ValidationError:
            self.errors.append(_("Please enter a valid duration for all activities."))
        # validar estes valores apenas se não houver já erros
        if posted and not self.errors and not self.steps and not self.activity_inputs:
            self.errors.append(_("Please enter something for at least one field."))
            
    def is_valid(self):
        return self.errors == []

class ActivityForm(forms.Form):
    activity = forms.CharField(label=_("Activity label"))
    steps = forms.IntegerField(label=_("Steps"),
                help_text=_("Equivalent number of steps for each minute of activity"))
    
    def clean_activity(self):
        name=self.cleaned_data['activity']
        query = GqlQuery("SELECT * FROM Activity WHERE name=:1", name)
        if query.count() > 0:
            raise forms.ValidationError(_("Activity already exists"))
        return self.cleaned_data['activity']

class ConfigForm(forms.Form):
    from_email = forms.EmailField(label=_("Email"), required=False, 
                 help_text=_("This email is the sender for messages sent to users. The email must also be in the list of appengine emails."))
    maps_key = forms.CharField(label=_("Google Maps Key"), required=False,
                help_text=_("With a valid key for your domain you can define the geographical coordinates of starting and finish point, the distance and path using google maps. You will also be able to see the user's current position and progress in a nice map."))
    allowed_domains = forms.CharField(label=_("Allowed domains"), required=False,  widget=forms.Textarea(),
                help_text=_("Comma separated list of domains accepted for new user registration (for example: mydomain.com, myotherdomain.com)"))
    bg_color = forms.CharField(label='Background color', required=False)
    bg_image = RemovableFileFormField(label='Background image', required=False,
                help_text=_("Maximum file size of %s kb" % int(settings.MAX_UPLOAD_SIZE / 1024)))
    bg_repeat = forms.BooleanField(label='Image repeat', required=False)
    link_color = forms.CharField(label='Link color', required=False)
    site_color1 = forms.CharField(label='Site color 1', required=False)
    site_color2 = forms.CharField(label='Site color 2', required=False)

    def __init__(self, *args, **kwargs):
        filename = ''
        if kwargs.has_key('initial') and kwargs['initial'].has_key('filename'):
            filename = kwargs['initial']['filename']
        logging.debug("FILENAME: " + str(filename))
        path = ''
        if kwargs.has_key('initial') and kwargs['initial'].has_key('path'):
            path = kwargs['initial']['path']
        logging.debug(path)
        super(ConfigForm, self).__init__(*args, **kwargs)
        self.fields['bg_image'].widget.set_filename(filename)
        self.fields['bg_image'].widget.set_path(path)
    
    def clean_bg_image(self):
        newfile = self.cleaned_data['bg_image'][0]
        if newfile and hasattr(settings, 'MAX_UPLOAD_SIZE') and newfile.size > settings.MAX_UPLOAD_SIZE:
            max_kbytes = int(settings.MAX_UPLOAD_SIZE / 1024)
            raise forms.ValidationError(_("Please use a file with size under %s kb.") % max_kbytes)
        return self.cleaned_data['bg_image']

class GoalForm(forms.Form):
    start = forms.CharField(label=_("Departure Town"))
    finish = forms.CharField(label=_("Arrival Town"))
    start_date = forms.DateField(label=_("Departure Date"), help_text=_("Date format (yyyy-mm-dd)"))
    end_date = forms.DateField(label=_("Arrival Date"), help_text=_("Date format (yyyy-mm-dd)"))
    total_distance = forms.FloatField(label=_("Total distance (km)"), min_value=0, max_value=999999)
    start_geo = forms.CharField(widget=forms.HiddenInput(), required=False)
    finish_geo = forms.CharField(widget=forms.HiddenInput(), required=False)
    directions = forms.CharField(widget=forms.HiddenInput(), required=False)
    
    #MORE CHECKS:
    #end date > start date
    
    def clean_end_date(self):
        if self.cleaned_data['end_date'] and self.cleaned_data['start_date']:
            if self.cleaned_data['start_date'] > self.cleaned_data['end_date']:
                raise forms.ValidationError(_("Arrival date must be after the departure date"))
        return self.cleaned_data['end_date']
    
    def map_geo(self, value):
        try:
            lat, lon = map(float,value.split(','))
            return GeoPt(lat, lon)
        except ValueError:
            return None
            
    def clean_start_geo(self):
        return self.map_geo(self.cleaned_data['start_geo'])
    
    def clean_finish_geo(self):
        return self.map_geo(self.cleaned_data['finish_geo'])
