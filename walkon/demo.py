# -*- coding: utf-8 -*-
from random import choice, randint
from google.appengine.ext import db
from django.http import HttpResponse
from models import Goal, Step, User
from Tester import get_name, DateGenerator
import StepConverter

def fillusers(request):
    """Crate a bunch of demo users."""
    for i in range(25):
        first, last = get_name().split(" ")
        username = "%s.%s" % (first,last)
        query = db.GqlQuery("SELECT * FROM User WHERE username = :1", username)
        if not query.count():
            email = "%s@test.com" % username
            user = User(username=username, email=email)
            user.first_name = first
            user.last_name = last
            user.save()

def fillstuff(request):
    #start = "Lisboa"
    #finish = "Moscovo"
    #total_distance = 64872
    #goal = Goal(start=start, finish=finish, start_date=start_date, end_date=end_date, total_distance=total_distance)
    #goal.save()
    goal_query = Goal.all()
    goal = goal_query.get()
    dg = DateGenerator(goal.start_date, goal.end_date)
    users_query = User.all()
    users = users_query.fetch(25) # More than enough for testing purposes
    activities = StepConverter.activities.keys()
    for i in range(2000):
        user = choice(users)
        steps = randint(1, 1000)
        desc = choice(activities)
        dist = StepConverter.convert_steps(steps)
        s = Step(user=user, steps=steps, distance=dist, date=dg.get_date(), description=desc)
        s.save()
    return HttpResponse("OK")

def getusers(request):
    users_query = User.all()
    users = [str(u.key()) for u in users_query.fetch(25)]
    return HttpResponse("\n".join(users))