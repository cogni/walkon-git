# coding: utf-8
import logging
import mimetypes
from urllib import urlencode
from datetime import date, timedelta, datetime
from django.conf import settings 
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django import forms
from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext as _
from google.appengine.api import users
from google.appengine.ext import db
from google.appengine.ext.db import GeoPt, Key
from google.appengine.api.labs import taskqueue
from google.appengine.api import memcache

from cogni.shortcuts import get_object_or_404
from userprofile.models import Theme
import StepConverter, Reports
from models import Goal, Step, Activity, User, GlobalConfig, StatsByActivity

WALKON_CONFIG_NAME = 'walkondefs'
ACTIVITIES_LIST_LEN = 10

def get_user_index(stats, userid):
    """list.index() throws a ValueError exception if it doesn't find the item."""
    return [s[0].id for s in stats].index(userid)

@login_required
def index(request):
    """Main view for the WalkOn application.

    Handles input for number of steps on the chosen date.
    """
    goal = memcache.get("goal")
    if goal is None:
        logging.info("Goal not in cache")
        goal_query = Goal.all()
        goal = goal_query.get()
        memcache.add("goal", goal, 3600)
    user = request.user
    if not goal:
        if user.is_superuser:
            return HttpResponseRedirect(reverse('cfg_goal'))
        else:
            return render_to_response('nogoal.html', {}, context_instance=RequestContext(request))

    # make sure user has statistics, even if it is the first login
    Reports.stats_sanity_check(goal, user)
    # handle main screen
    #activities_query.order('default')
    from walkon.forms import DateForm, StepForm
    # handle input
    # The date is usually chosen on the client side thanks to JS. If the user has JS disabled, then a click
    # on the tabs will reload the page with the new date as a query parameter. If the user clicks on the calendar
    # tab, he can enter the new date into an input text box.
    if request.method == 'GET' and request.GET.has_key('date'):
        date_form = DateForm(request.GET)
        if date_form.is_valid():
            current_date = date_form.cleaned_data['date']
        else:
            current_date = date.today()
    else:
        current_date = date.today()
        date_form = DateForm({'date': current_date, 'show_cal': 1 })
    # Generate the form for step input dynamically, based on the current list of activities
    activities_top_10_text = {}
    # Aaargh :-/ what a mess
    act_user = memcache.get("act_"+user.username)
    if act_user is None:
        act_user_query = db.GqlQuery("SELECT * FROM StatsByActivity WHERE user = :1 ORDER BY distance DESC", user)
        act_user = act_user_query.fetch(ACTIVITIES_LIST_LEN+1)
        memcache.add("act_"+user.username, act_user, 3600)
    for act in act_user:
        if act.activity and len(activities_top_10_text)<ACTIVITIES_LIST_LEN:
            activities_top_10_text[act.activity.key()] = act.activity
    activities_query = Activity.all()
    activities_query.order('name')
    #logging.info("%s - %d" % (",".join(activities_top_10_text.keys()), len(activities_top_10_text)))
    if len(activities_top_10_text) < ACTIVITIES_LIST_LEN:
        missing = ACTIVITIES_LIST_LEN - len(activities_top_10_text)
        act_global = memcache.get("act_global")
        if act_global is None:
            act_query = db.GqlQuery("SELECT * FROM GlobalStatsByActivity ORDER BY distance DESC")
            act_global = act_query.fetch(ACTIVITIES_LIST_LEN)
            memcache.add("act_global", act_global, 3600)
        for act in act_global:
            if missing == 0:
                break
            if act.activity and act.activity.key() not in activities_top_10_text:
                activities_top_10_text[act.activity.key()] = act.activity
                missing -= 1
                #logging.info("%s - %d" %(act.description, missing))
        if missing:
            act_default = memcache.get("act_default")
            if act_default is None:
                act_default = activities_query.fetch(ACTIVITIES_LIST_LEN)
                memcache.add("act_default", act_default, 3600)
            for act in act_default:
                if missing == 0:
                    break
                if act.key() not in activities_top_10_text:
                    activities_top_10_text[act.key()] = act
                    missing -= 1
                    #logging.info("%s - %d" %(act.name, missing))
    # running time cost is here
    # still, it will make ACTIVITIES_LIST_LEN queries to the datastore
    #top_10_key = "act10_" + "".join(activities_top_10_text.keys())
    #activities_top_10 = memcache.get(top_10_key)
    #if activities_top_10 is None:
    #    act_10_query = db.GqlQuery("SELECT * FROM Activity WHERE name IN :1 ORDER BY name", activities_top_10_text.keys())
    #    activities_top_10 = act_10_query.fetch(ACTIVITIES_LIST_LEN)
    #    memcache.add(top_10_key, activities_top_10, 3600)
    activities_top_10 = [ (a[1].name, a[1]) for a in activities_top_10_text.items() ]
    logging.debug(str(activities_top_10))
    activities_top_10.sort()
    logging.debug(str(activities_top_10))
    activities_top_10 = [a[1] for a in activities_top_10]
    #activities_top_10 = [act for act in activities_query if act.name in activities_top_10_text]
    if request.method == 'POST':
        if request.POST.has_key('steps'):
            step_form = StepForm(request.POST, activities=activities_top_10)
            if step_form.is_valid():
                current_date = step_form.date_hidden
                activity_inputs = step_form.activity_inputs
                steps = step_form.steps
                steps_added = False
                step_list = []
                if steps:
                    dist = StepConverter.convert_steps(steps)
                    step = Step(date=current_date, steps=steps, distance=dist, user=user)
                    step_list.append(step)
                    steps_added = True
                # now see if activities are present
                for key, time in activity_inputs:
                    steps, act = StepConverter.convert_act(time, key)
                    dist = StepConverter.convert_steps(steps)
                    step = Step(date=current_date, steps=steps, distance=dist, user=user, activity=act)
                    step_list.append(step)
                db.put(step_list)
                Reports.update_stats(goal=goal, user=user)
                # invalidate these keys
                memcache.delete("act_"+user.username)
                memcache.delete("act_global")
                if steps_added:
                    return HttpResponseRedirect('%s?%s' % (reverse("index"), urlencode({'date': current_date, 'save': 'ok'})))
                else:
                    return HttpResponseRedirect('%s?%s' % (reverse("index"), urlencode({'date': current_date, 'msg': _("Your changes have been saved")})))
            else:
                if step_form.date_hidden:
                    current_date = step_form.date_hidden
        else:
            # check if we're deleting info
            delete_keys = [db.Key(step[3:]) for step in request.POST.keys() if step.startswith('del')]
            if delete_keys:
                logging.info('Deleting %d keys' % len(delete_keys))
                db.delete(delete_keys)
                msg = "Steps deleted!"
                Reports.update_stats(goal=goal, user=user)
                current_date = request.POST.get('date_hidden', current_date)
                memcache.delete("act_"+user.username)
                memcache.delete("act_global")
                return HttpResponseRedirect('%s?%s' % (reverse("index"), urlencode({'date': current_date, 'msg': msg})))
            else:
                msg = ''
                StepForm = StepForm(activities=activities_top_10, initial={'date_hidden': current_date})
    else:
        step_form = StepForm(activities=activities_top_10, initial={'date_hidden': current_date})
    # Handle all the other stuff needed for the page
    stats_date = date.today() > goal.end_date and goal.end_date or date.today()
    stats = Reports.personal_current(stats_date, goal)
    if stats:
        whoami_idx = get_user_index(stats, user.id)
        # E se existirem mais de 10 users e o user não estiver no top 5, mostrar uma outra caixa com o user com os 5 anteriores e 4 seguintes.
        if len(stats) > 10 and whoami_idx > 5:
            hrank = Reports.hrank(stats, whoami_idx, 5)
        else:
            hrank = None
        line = Reports.line(stats, whoami_idx)
    else:
        line = None
        hrank = None
    error_msg = request.GET.get('error', "")
    info_msg = request.GET.get('msg', "")
    save = request.GET.get('save', "")
    show_cal = request.GET.get('show_cal', False)
    date_2 = date.today() - timedelta(2)
    date_1 = date.today() - timedelta(1)
    istoday = current_date == date.today()
    isday1 = current_date == date_1
    isday2 = current_date == date_2
    isotherday = not (istoday or isday1 or isday2)
    # step table
    steps_query = db.GqlQuery("SELECT * FROM Step WHERE date = :1 AND user = :2", current_date, user)
    # Is anyone going to enter over 100 registers/day?
    es = [(st.activity and st.activity.name or StepConverter.COUNTER_DESCRIPTION, st) for st in steps_query.fetch(1000)]
    es.sort()
    es = [st[1] for st in es]
    return render_to_response('index.html', {'date': current_date, 'day_2': date_2, 'day_1': date_1, 'activities' : activities_query.count() > ACTIVITIES_LIST_LEN, 'entered_steps': es, 'personal_stats': stats[:10], 'stats_hrank': hrank, 'date_form': date_form, 'error': error_msg , 'info': info_msg , 'save': save, 'summary': line, 'step_form': step_form, 'show_cal': show_cal, 'istoday': istoday, 'isday1': isday1, 'isday2': isday2, 'isotherday': isotherday, 'whoami': user}, context_instance=RequestContext(request))

@login_required
def get_step_table(request):
    """Returns the table with the entries for the chosen date. 
    
    Called by AJAX when picking a date on the main screen.
    """
    user = request.user
    if request.method == 'GET' and request.GET.has_key('date'):
        from walkon.forms import DateForm
        date_form = DateForm(request.GET)
        if date_form.is_valid():
            current_date = date_form.cleaned_data['date']
            steps_query = db.GqlQuery("SELECT * FROM Step WHERE date = :1 AND user = :2", current_date, user)
            # Is anyone going to enter over 100 registers/day?
            es = [(st.activity and st.activity.name or StepConverter.COUNTER_DESCRIPTION, st) for st in steps_query.fetch(1000)]
            es.sort()
            es = [st[1] for st in es]
            return render_to_response('step_table.html', {'date': current_date, 'entered_steps': es}, context_instance=RequestContext(request))
    logging.debug("Error on get_step_table: %s!" % request.GET.get('date', '<date missing>'))

@login_required
def get_full_step_form(request):
    """Returns the full activites form.
    
    Call by AJAX when clicking on "show all activities" on the index page.
    """
    user = request.user
    from walkon.forms import DateForm, StepForm
    activities_query = Activity.all()
    activities_query.order('name')
    if request.method == 'GET' and request.GET.has_key('date'):
        date_form = DateForm(request.GET)
        if date_form.is_valid():
            current_date = date_form.cleaned_data['date']
        else:
            logging.debug("Error on get_full_step_form: %s!" % request.GET.get('date', '<date missing>'))
            return HttpResponse("") # throw exception?
    else:
        current_date = date.today()
    step_form = StepForm(activities=activities_query, initial={'date_hidden': current_date})
    return render_to_response('step_form.html', {'step_form': step_form}, context_instance=RequestContext(request))
    

@login_required
def reports(request):
    """Will receive (get) date=today, person=current_user
    
    For now I'll always get the whole list and work on it. If necessary, we'll optimize later
    """
    user = request.user
    goal_query = Goal.all()
    goal = goal_query.get()
    from walkon.forms import DateForm
    if (request.GET.has_key('date')):
        date_form = DateForm(request.GET)
        if date_form.is_valid():
            current_date = date_form.cleaned_data['date']
        else:
            current_date = date.today()
    else:
        current_date = date.today()
        date_form = DateForm(initial={'date': current_date, 'show_cal': 1 })
    if (request.GET.has_key('person')):
        whoami = User.get(request.GET['person'])
    else:
        whoami = user
    config = GlobalConfig.get_or_insert(WALKON_CONFIG_NAME)

    if current_date > goal.end_date:
        current_date = goal.end_date

    days = (current_date - goal.start_date).days + 1 # includes both today and 1st day
    days_remaining = (goal.end_date-current_date).days
    res = Reports.personal_current(current_date, goal)
    if not res:
        return render_to_response('reports.html', {"goal": goal, 'thisdate': current_date, 'error': 'No one has moved yet.', 'date_form': date_form, 'user': user, 'whoami': whoami, 'isadmin': user.is_superuser, 'config': config}, context_instance=RequestContext(request))
    whoami_idx = get_user_index(res, whoami.id)
    summary = Reports.summary(res, whoami_idx)
    return render_to_response('report-map.html', {"goal": goal, 'thisdate': current_date, 'stats_summary': summary, 'date_form': date_form, 'user': user, 'whoami': whoami, 'isadmin': user.is_superuser, 'config': config}, context_instance=RequestContext(request))


def graph_by_activity(res_by_activity):
    """If we have more than 10 activites, show the top 9 and the cumulative for the others to avoid 
    "request too big" errors """
    if len(res_by_activity) > 10:
        others = res_by_activity[9:]
        others_data = reduce(lambda x,y: (None, x[1]+y[1],x[2]+y[2]),others)
        trash, total, percentage = others_data
        res_by_activity = res_by_activity[:9] + [("Others", total, percentage)]
    return res_by_activity
            
@login_required
def reports_activities(request):
    """Will receive (get) date=today, person=current_user
    
    Distribution of distance by activity
    """
    user = request.user
    goal_query = Goal.all()
    goal = goal_query.get()
    from walkon.forms import DateForm
    if (request.GET.has_key('date')):
        date_form = DateForm(request.GET)
        if date_form.is_valid():
            current_date = date_form.cleaned_data['date']
        else:
            current_date = date.today()
    else:
        current_date = date.today()
        date_form = DateForm(initial={'date': current_date, 'show_cal': 1 })
    if (request.GET.has_key('person')):
        whoami = User.get(request.GET['person'])
    else:
        whoami = user
    config = GlobalConfig.get_or_insert(WALKON_CONFIG_NAME)

    if current_date > goal.end_date:
        current_date = goal.end_date

    days = (current_date - goal.start_date).days + 1 # includes both today and 1st day
    days_remaining = (goal.end_date-current_date).days
    res = Reports.personal_current(current_date, goal)
    if not res:
        return render_to_response('report-activity.html', {"goal": goal, 'thisdate': current_date, 'error': 'No one has moved yet.', 'date_form': date_form, 'user': user, 'whoami': whoami, 'isadmin': user.is_superuser, 'config': config}, context_instance=RequestContext(request))
    whoami_idx = get_user_index(res, whoami.id)
    
    res_by_activity = Reports.by_activity(res)
    personal_by_activity = Reports.by_activity(res, whoami_idx)
    res_by_activity_graph = graph_by_activity(res_by_activity)
    personal_by_activity_graph = graph_by_activity(personal_by_activity)
    pie = {'me': [act[2] for act in personal_by_activity_graph], 'me_labels': [act[0] for act in personal_by_activity_graph], 'global': [act[2] for act in res_by_activity_graph], 'global_labels': [act[0] for act in res_by_activity_graph]}
    return render_to_response('report-activity.html', {"goal": goal, 'thisdate': current_date, 'stats_pie': pie, 'by_activity': res_by_activity, 'my_activity': personal_by_activity, 'date_form': date_form, 'user': user, 'whoami': whoami, 'isadmin': user.is_superuser, 'config': config, 'hide_date': True}, context_instance=RequestContext(request))
    
@login_required
def reports_benchmark(request):
    """Will receive (get) date=today, person=current_user
    
    For now I'll always get the whole list and work on it. If necessary, we'll optimize later
    """
    user = request.user
    goal = memcache.get("goal")
    if goal is None:
        goal_query = Goal.all()
        goal = goal_query.get()
        memcache.add("goal", goal, 3600)
    from walkon.forms import DateForm
    if (request.GET.has_key('date')):
        date_form = DateForm(request.GET)
        if date_form.is_valid():
            current_date = date_form.cleaned_data['date']
        else:
            current_date = date.today()
    else:
        current_date = date.today()
        date_form = DateForm(initial={'date': current_date, 'show_cal': 1 })
    if (request.GET.has_key('person')):
        whoami = User.get(request.GET['person'])
    else:
        whoami = user
    config = GlobalConfig.get_or_insert(WALKON_CONFIG_NAME)

    if current_date > goal.end_date:
        current_date = goal.end_date

    days = (current_date - goal.start_date).days + 1 # includes both today and 1st day
    days_remaining = (goal.end_date-current_date).days
    res = Reports.personal_current(current_date, goal)
    if not res:
        return render_to_response('report-ranking.html', {"goal": goal, 'thisdate': current_date, 'error': 'No one has moved yet.', 'date_form': date_form, 'user': user, 'whoami': whoami, 'isadmin': user.is_superuser, 'config': config}, context_instance=RequestContext(request))
    whoami_idx = get_user_index(res, whoami.id)
    
    summary = Reports.summary(res, whoami_idx)
    stats_me = [ (ws.cumulative_distance, ws.daily_distance) for ws in Reports.daily_stats(res, whoami_idx, current_date, goal.start_date)]
    line_cumul_me, bar_me = zip(*stats_me)     
    #line_cumul_me = Reports.daily_cumulative(res, whoami_idx, current_date, goal.start_date)
    stats_top = [ (ws.cumulative_distance, ws.daily_distance) for ws in Reports.daily_stats(res, 0, current_date, goal.start_date)]
    line_cumul_top, bar_top = zip(*stats_top)
    #line_cumul_top = Reports.daily_cumulative(res, 0, current_date, goal.start_date)
    line_cumul_avg = Reports.daily_cumulative_avg(res, current_date, goal.start_date)
    stats_line = {
        'me': line_cumul_me, 'me_max': line_cumul_me[-1],
        'top': line_cumul_top, 'top_max': line_cumul_top[-1],
        'avg': line_cumul_avg, 'avg_max': line_cumul_avg[-1],
    }
    #bar_me = Reports.daily_distances(res, whoami_idx, current_date)
    #bar_top = Reports.daily_distances(res, 0, current_date)
    bar_avg = Reports.daily_distances_avg(res, current_date, goal.start_date)
    stats_bar = {
        'me': bar_me,
        'me_max': max(bar_me),
        'top': bar_top,
        'top_max': max(bar_top),
        'avg': bar_avg,
        'avg_max': max(bar_avg),
        'labelsx': [(not x%3) and (goal.start_date+timedelta(x)).isoformat() or '' for x in range(days)]
    }
    return render_to_response('report-benchmark.html', {"goal": goal, 'thisdate': current_date, 'stats_summary': summary, 'stats_line': stats_line, 'stats_bar': stats_bar,  'date_form': date_form, 'user': user, 'whoami': whoami, 'isadmin': user.is_superuser, 'config': config}, context_instance=RequestContext(request))
    
@login_required
def reports_projections(request):
    """Will receive (get) date=today, person=current_user
    
    For now I'll always get the whole list and work on it. If necessary, we'll optimize later
    """
    user = request.user
    goal_query = Goal.all()
    goal = goal_query.get()
    from walkon.forms import DateForm
    if (request.GET.has_key('date')):
        date_form = DateForm(request.GET)
        if date_form.is_valid():
            current_date = date_form.cleaned_data['date']
        else:
            current_date = date.today()
    else:
        current_date = date.today()
        date_form = DateForm(initial={'date': current_date, 'show_cal': 1 })
    if (request.GET.has_key('person')):
        whoami = User.get(request.GET['person'])
    else:
        whoami = user
    config = GlobalConfig.get_or_insert(WALKON_CONFIG_NAME)

    if current_date > goal.end_date:
        current_date = goal.end_date

    days = (current_date - goal.start_date).days + 1 # includes both today and 1st day
    days_remaining = (goal.end_date-current_date).days
    res = Reports.personal_current(current_date, goal)
    if not res:
        return render_to_response('report-ranking.html', {"goal": goal, 'thisdate': current_date, 'error': 'No one has moved yet.', 'date_form': date_form, 'user': user, 'whoami': whoami, 'isadmin': user.is_superuser, 'config': config}, context_instance=RequestContext(request))
    whoami_idx = get_user_index(res, whoami.id)
    
    # Top 10
    projections = Reports.projections(res, whoami_idx, days, days_remaining, goal)
    return render_to_response('report-projections.html', {"goal": goal, 'thisdate': current_date, 'projections': projections, 'date_form': date_form, 'user': user, 'whoami': whoami, 'isadmin': user.is_superuser, 'config': config}, context_instance=RequestContext(request))
    
@login_required
def reports_rankings(request):
    """Will receive (get) date=today, person=current_user
    
    For now I'll always get the whole list and work on it. If necessary, we'll optimize later
    """
    user = request.user
    goal_query = Goal.all()
    goal = goal_query.get()
    from walkon.forms import DateForm
    if (request.GET.has_key('date')):
        date_form = DateForm(request.GET)
        if date_form.is_valid():
            current_date = date_form.cleaned_data['date']
        else:
            current_date = date.today()
    else:
        current_date = date.today()
        date_form = DateForm(initial={'date': current_date, 'show_cal': 1 })
    if (request.GET.has_key('person')):
        whoami = User.get(request.GET['person'])
    else:
        whoami = user
    config = GlobalConfig.get_or_insert(WALKON_CONFIG_NAME)

    if current_date > goal.end_date:
        current_date = goal.end_date

    days = (current_date - goal.start_date).days + 1 # includes both today and 1st day
    days_remaining = (goal.end_date-current_date).days
    res = Reports.personal_current(current_date, goal)
    if not res:
        return render_to_response('report-ranking.html', {"goal": goal, 'thisdate': current_date, 'error': 'No one has moved yet.', 'date_form': date_form, 'user': user, 'whoami': whoami, 'isadmin': user.is_superuser, 'config': config}, context_instance=RequestContext(request))
    whoami_idx = get_user_index(res, whoami.id)
    
    # Top 10
    top_10 = res[:10]
    hrank = Reports.hrank(res, whoami_idx)
    return render_to_response('report-ranking.html', {"goal": goal, 'thisdate': current_date, 'stats': res, 'stats_top10': top_10, 'stats_hrank': hrank, 'date_form': date_form, 'user': user, 'whoami': whoami, 'isadmin': user.is_superuser, 'config': config}, context_instance=RequestContext(request))
    
@login_required
def reports_steps(request):
    """Will receive (get) date=today, person=current_user
    
    For now I'll always get the whole list and work on it. If necessary, we'll optimize later
    """
    user = request.user
    goal_query = Goal.all()
    goal = goal_query.get()
    from walkon.forms import DateForm
    if (request.GET.has_key('date')):
        date_form = DateForm(request.GET)
        if date_form.is_valid():
            current_date = date_form.cleaned_data['date']
        else:
            current_date = date.today()
    else:
        current_date = date.today()
        date_form = DateForm(initial={'date': current_date, 'show_cal': 1 })
    if (request.GET.has_key('person')):
        whoami = User.get(request.GET['person'])
    else:
        whoami = user
    config = GlobalConfig.get_or_insert(WALKON_CONFIG_NAME)

    if current_date > goal.end_date:
        current_date = goal.end_date

    days = (current_date - goal.start_date).days + 1 # includes both today and 1st day
    days_remaining = (goal.end_date-current_date).days
    
    # Read the user's Step entry list for the whole competition
    steps_query = db.GqlQuery("SELECT * FROM Step WHERE user = :1 ORDER BY date DESC", user)
    # Is anyone going to enter over 100 registers/day?
    steps = steps_query.fetch(1000)
    return render_to_response('report-steps.html', {"goal": goal, 'thisdate': current_date, 'steps': steps, 'date_form': date_form, 'user': user, 'whoami': whoami, 'isadmin': user.is_superuser, 'config': config}, context_instance=RequestContext(request))
    

@login_required
def full_results(request):
    """Show full table"""
    user = request.user
    whoami = user
    from walkon.forms import DateForm
    if (request.GET.has_key('date')):
        date_form = DateForm(request.GET)
        if date_form.is_valid():
            current_date = date_form.cleaned_data['date']
        else:
            current_date = date.today()
    else:
        current_date = date.today()
        date_form = DateForm(initial={'date': current_date, 'show_cal': 1 })
    if (request.GET.has_key('person')):
        whoami = User.get(request.GET['person'])
    else:
        whoami = user
    config = GlobalConfig.get_or_insert(WALKON_CONFIG_NAME)
    goal_query = Goal.all()
    goal = goal_query.get()
    if current_date > goal.end_date:
        current_date = goal.end_date
    days = (current_date - goal.start_date).days + 1 # includes both today and 1st day
    days_remaining = (goal.end_date-current_date).days
    res = Reports.personal_current(current_date, goal)
    if not res:
        return render_to_response('full_table.html', {"goal": goal, 'thisdate': current_date, 'error': 'No one has moved yet.', 'date_form': date_form, 'user': user, 'whoami': whoami, 'isadmin': user.is_superuser, 'config': config}, context_instance=RequestContext(request))
    res = [(x,)+r for x,r in map(None, range(1,len(res)+1),res)]
    return render_to_response('full_table.html', {"goal": goal, 'thisdate': current_date, 'stats_hrank': res, 'user': user, 'whoami': whoami, 'isadmin': user.is_superuser, 'config': config}, context_instance=RequestContext(request))

@login_required
def populate_stats(request):
    user = request.user
    if (not user.is_superuser):
        return HttpResponseRedirect(reverse("index"))
    if request.GET.has_key('date'):
        day = datetime.strptime(request.GET['date'], "%Y-%m-%d").date()
    else:
        day = None
    goal_query = Goal.all()
    goal = goal_query.get()
    res = Reports.populate_stats(goal, day)
    return HttpResponse("Done!")

def update_user_stats(request):
    user = request.POST['key']
    if user:
        goal_query = Goal.all()
        goal = goal_query.get()
        u = User.get(user)
    res = Reports.update_stats(goal, u)
    return HttpResponse("Done!")

def crop_stats(request):
    goal_query = Goal.all()
    goal = goal_query.get()
    res = Reports.crop_stats(goal)
    return HttpResponse("Done!")
    
    
# Admin views

@login_required
def global_config(request):
    from walkon.forms import ConfigForm
    user = request.user
    if not user.is_superuser:
        return HttpResponseRedirect(reverse("index"))
    goal_query = Goal.all()
    goal = goal_query.get()
    config = GlobalConfig.get_or_insert(WALKON_CONFIG_NAME, allowed_domains=[])
    if request.method == 'POST':
        form = ConfigForm(request.POST, request.FILES, initial={'filename': config.bg_name, 'path': reverse('global_theme_image', args=('',))})
        if form.is_valid():
            config.maps_key = form.cleaned_data['maps_key']
            # empty email address would raise a BadValueException
            if form.cleaned_data['from_email']:
                config.from_email = form.cleaned_data['from_email']
            config.allowed_domains = form.cleaned_data['allowed_domains']
            if form.cleaned_data['bg_image'] and form.cleaned_data['bg_image'][0]:
                config.bg_name = form.cleaned_data['bg_image'][0].name
                config.bg_image = form.cleaned_data['bg_image'][0].read()
            elif form.cleaned_data['bg_image'] and form.cleaned_data['bg_image'][1]:
                config.bg_name = ''
                config.bg_image = None
            config.bg_color = form.cleaned_data['bg_color']
            config.link_color = form.cleaned_data['link_color']
            config.site_color1 = form.cleaned_data['site_color1']
            config.site_color2 = form.cleaned_data['site_color2']
            config.bg_repeat = form.cleaned_data['bg_repeat']
            config.save()
            return HttpResponseRedirect('%s?save=ok' % reverse('cfg_main'))
    else:
        thm = request.GET.get('theme','')
        if thm == 'default':
            tm = None
        elif thm:
            tm = Theme.all().filter('name =', thm).get()
        else:
            tm = None
        if thm:
            config.theme = tm
            config.bg_repeat = False
            config.bg_color = user.filename = user.bg_color = ''
            config.bg_image = None
            config.link_color = user.site_color1 = user.site_color2 = ''
            config.save()
            msg = _("Theme changed")
        
        form = ConfigForm(initial={'from_email': config.from_email, 'maps_key':config.maps_key,
                                    'allowed_domains': config.allowed_domains, 'bg_color': config.bg_color, 'bg_repeat': config.bg_repeat, 
                                    'filename': config.bg_name, 'path': reverse('global_theme_image', args=('',)), 
                                    'link_color': config.link_color, 'site_color1': config.site_color1,
                                    'site_color2': config.site_color2 })
    save = request.GET.get('save', False)
    return render_to_response('globalconfig.html', {'form': form, 'save': save, 'globalconfig': config, 
                'themes': Theme.all()}, 
                context_instance=RequestContext(request))

def queue_update_stats():
    taskqueue.add(url='/cropstats')
    for u in User.all():
        # Add the task to the default queue.
        taskqueue.add(url='/updatestats', params={'key': u.id})


@login_required
def goal(request):
    from walkon.forms import GoalForm
    user = request.user
    if not user.is_superuser:
        return HttpResponseRedirect(reverse("index"))
    goal_query = Goal.all()
    goal = goal_query.get()
    info = goal and goal.start_date <= date.today() or False
    save = request.GET.get('save', False)
    config = GlobalConfig.get_or_insert(WALKON_CONFIG_NAME)
    fields = ('start', 'finish', 'start_date', 'end_date', 'total_distance', 'start_geo', 'finish_geo', 'directions')
    if request.method == 'POST':
        form = GoalForm(request.POST)
        if form.is_valid():
            if not goal:
                fdict = dict([(key,form.cleaned_data[key]) for key in fields])
                goal = Goal(**fdict)
            else:
                [setattr(goal, key, form.cleaned_data[key]) for key in fields]
            goal.save()
            # add tasks to update user stats to queue
            queue_update_stats()
            memcache.set("goal", goal, 3600)
            return HttpResponseRedirect('%s?save=ok' % reverse('cfg_goal'))
    else:
        if goal:
            fdict = dict([(key, getattr(goal,key)) for key in fields])
        else:
            fdict = {}
        form = GoalForm(initial=fdict)
    return render_to_response('goal.html', {'form': form, 'config': config, 'info': info, 'save': save}, 
            context_instance=RequestContext(request))

@login_required
def activities(request):
    from walkon.forms import ActivityForm
    user = request.user
    if not user.is_superuser:
        return HttpResponseRedirect(reverse("index"))
    activities_query = Activity.all().order('name')
    msg = None
    if request.method == 'POST':
        form = ActivityForm(request.POST)
        delete_keys = [act[3:] for act in request.POST.keys() if act.startswith('del')]
        if delete_keys:
            for act in map(Activity.get, delete_keys):
                verify_query = db.GqlQuery("SELECT * FROM Step WHERE activity = :1", act)
                if verify_query.count():
                    msg = "Cannot delete - users have already entered data for this activity!"
                    return HttpResponseRedirect('%s?%s' % (reverse('cfg_activities'), urlencode({'msg': msg})))
            db.delete(map(db.Key, delete_keys))
            msg = "Activity deleted!"
            return HttpResponseRedirect('%s?%s' % (reverse('cfg_activities'), urlencode({'msg': msg})))
        else:
            if form.is_valid():
                activity = Activity(name=form.cleaned_data['activity'], steps_min=form.cleaned_data['steps'])
                activity.save()
                msg = "Activity added successfully!"
                return HttpResponseRedirect('%s?%s' % (reverse('cfg_activities'), urlencode({'msg': msg})))
    else:
        form = ActivityForm()
        msg = request.GET.get('msg','')
    return render_to_response('activities.html', {'activities': activities_query, 'form': form, 'msg': msg}, 
                            context_instance=RequestContext(request))

@login_required
def userpage(request, username):
    member = get_object_or_404(User, 'username =', username)
    goal_query = Goal.all()
    goal = goal_query.get()
    stats_date = date.today() > goal.end_date and goal.end_date or date.today()
    stats = Reports.personal_current(stats_date, goal)
    if stats:
        whoami_idx = get_user_index(stats, member.id)
        hrank = Reports.hrank(stats, whoami_idx, 5)
        summary = Reports.summary(stats, whoami_idx)
        line_cumul_me = Reports.daily_cumulative(stats, whoami_idx, stats_date, goal.start_date)
        line_cumul_top = Reports.daily_cumulative(stats, 0, stats_date, goal.start_date)
        line_cumul_avg = Reports.daily_cumulative_avg(stats, stats_date, goal.start_date)
        stats_line = {
            'me': line_cumul_me, 'me_max': line_cumul_me[-1],
            'top': line_cumul_top, 'top_max': line_cumul_top[-1],
            'avg': line_cumul_avg, 'avg_max': line_cumul_avg[-1],
        }
    else:
        hrank = None
        stats_line = None
    ctx = { 'member': member, 'stats_hrank': hrank, 'whoami': member, 'stats_line': stats_line }
    return render_to_response('userpage.html', ctx, RequestContext(request))

def global_theme_image(request, filename):
    config = GlobalConfig.get_or_insert(WALKON_CONFIG_NAME)
    if config.bg_image is not None:
        mt = mimetypes.guess_type(config.bg_filename)[0]
        return HttpResponse(config.bg_image, mimetype=mt)
    raise Http404

# Initializations

def fillactivities(request):
    """Only fills the datastore if no activities are present"""
    fill_themes()
    act_query = Activity.all()
    if act_query.count() == 0:
        for name,sm in StepConverter.activities.items():
            act = Activity(key_name=name, name=name, steps_min=sm)
            act.save()
    return HttpResponseRedirect(reverse("index"))

def fill_themes():
    """Adds the default themes if the themes table is empty"""
    from userprofile.models import Theme
    themes = Theme.all()
    if themes.count() == 0:
        t1 = Theme(name='snowboard', bg_image_path='/img/snowboard.jpg', thumbnail_path='/img/small_snowboard.jpg',
                   bg_color='#E5ECF9', bg_repeat=True, site_color1='#C3D9FF', site_color2='#E5ECF9', link_color='#588cb8')
        t1.save()
        t2 = Theme(name='mountain', bg_image_path='/img/bg_mountain.jpg', thumbnail_path='/img/small_mountain.jpg',
                   bg_color='#00271c', bg_repeat=True, site_color1='#cccc66', site_color2='#fff', link_color='#666633')
        t2.save()

def clear_stats(request):
    res = Reports.clear_stats()
    return HttpResponse("Done!")
    
def clear_steps(self):
    """ NOTE Limit of 1000 steps applies here as usual """
    from Reports import remove_by_query
    query = Step.all()
    remove_by_query(query)
    return HttpResponse("Done!")
    
def create_config(request):
    user = request.user
    if (not user.is_authenticated or not user.is_superuser):
        return HttpResponseRedirect(LOGIN_URL)
    config = GlobalConfig(key_name=WALKON_CONFIG_NAME, maps_key='ABQIAAAA0N3PiPaWoXv33S2eqZb9nxRMETvsGNCAndfWVhZYmMC7uZjZkxTZ97if6iLndP9I0x_dTgd_sq7H-Q', from_email=settings.ADMINS[0][1])
    config.put()
    
def initialization(request):
    gc = GlobalConfig.all().get()
    if not gc:
        create_config(request)
        fillactivities(request)
        fill_themes(request)
    return HttpResponseRedirect(reverse("index"))