# -*- coding: utf-8 -*-
from random import choice, randint
import datetime

names = ["Ana", "Bruno", "Carla", "Daniel", "Ema", "Francisco", "Luis", "Manuel", "Nuno", "Oscar", "Pedro"]
surnames = ["Garrido", "Henriques", "Inocente", "Juvenal", "Kelvin", "Querido", "Rodrigues", "Silva", "Tavares", "Urano"]

def get_name():
    return "%s %s" % (choice(names), choice(surnames))


class DateGenerator():
    """Generate random dates inside an interval
    
    Attributes:
        min_date, max_date
    """
    
    def __init__(self, min_date, max_date):
        """Arguments: min_date, max_date - datetime.date() objects"""
        self.min_date = min_date.toordinal()
        self.max_date = max_date.toordinal()
        
    def get_date(self):
        rnddate = randint(self.min_date, self.max_date)
        return datetime.date.fromordinal(rnddate)

if __name__ == "__main__":
    print get_name()