# coding: utf-8
import datetime, logging

from google.appengine.ext import db
from google.appengine.api import memcache

from models import Goal, DailyStats, StatsByActivity, GlobalStatsByActivity
import StepConverter
from userprofile.models import User

def remove_by_query(query):
    while True:
        res = query.fetch(1000)
        if not res: break
        db.delete(res)
        
def stats_sanity_check(goal, user):
    """Fills the users stats if empty for the current day. 
    
    Why the current day? When you edited the goal postponing the arrival date, stats for those additional days
    were not created, thus appearing as if noone had moved. Of course this only make sense if today is not past the end date."""
    today = datetime.date.today()
    if today > goal.end_date:
        today = goal.end_date
    query = DailyStats.all().filter("user =", user).filter("date =", today)
    if not query.get():
        update_stats(goal, user)
   
#def personal(self):
    #query = db.GqlQuery("SELECT * FROM Step ORDER by user")
    ## key = user; value = (total_steps, total_distance)
    #goal = Goal.all().get()
    #total_distance = goal.total_distance
    ## What if we're paste the finish date?
    #days = (datetime.date.today() - goal.start_date).days + 1 # includes both today and 1st day
    #stats = {}
    #current_user = (None, None)
    #for step in query:
        #if step.user.key() != current_user[0]:
            #current_user = (step.user.key(), step.user.username)
            #stats[current_user[1]] = (0,0)
        #old_steps, old_distance = stats[current_user[1]]
        #stats[current_user[1]] = (old_steps+step.steps, old_distance+step.distance)
    #res = [(total, (user,)+total+(total[0]/days, total[1]/total_distance)) for user, total in stats.items()]
    #res.sort()
    #return [stats for total, stats in res]

def personal_current(today, goal, top=1000):
    """res: (user, distance, current pace, % of path travelled so far) """
    query = db.GqlQuery("SELECT * FROM DailyStats WHERE date = :1 ORDER by cumulative_distance DESC", today)
    start_date = goal.start_date
    end_date = goal.end_date
    total_distance = goal.total_distance
    days = (today - start_date).days + 1 # includes both today and 1st day
    res = [(stat.user,stat.cumulative_distance,stat.cumulative_distance/days,stat.cumulative_distance*100/total_distance) for stat in query.fetch(top)]
    return res
    
def hrank(res, idx, surround=3):
    """data for rankings table"""
    total = len(res)
    start = idx-surround
    end = idx+surround
    if (start < 0):
        end = (2*surround)
    if end > total:
        end = total
        start = end-(2*surround)
    if (start < 0):
        start = 0
    try:
        return [(x,)+r for x,r in map(None, range(start+1,end+1),res[start:end])]
    except Exception, e:
        logging.error("%d:%d:%d:%d", start, end, total, idx)
        raise e


def get_average(res):
    """calculates the average distance travelled by the players"""
    total = len(res)
    return [None, None] + [i/total for i in map(lambda x: reduce(float.__add__,x),zip(*[i[1:] for i in res]))]
    
def summary(res, idx):
    """(#, user, km, km/day, %)
    
    res = comes from personal_current
    """
    me = (idx+1,)+res[idx]
    total = len(res)
    first = (1,) + res[0]
    last = (total,) + res[-1]
    average = get_average(res)
    if me[2] >= average[2]:
        ret = [first, me, average, last]
    else:
        ret = [first, average, me, last]
    return ret

def line(res, idx):
    """(user, %line) for (first, me, avg)"""
    me = (idx+1,)+res[idx]
    first = (1,) + res[0]
    # To get average only of people that have moved uncomment the line
    #res = filter(lambda x: x[1], res)
    #average = [None, None] + [i/total for i in map(lambda x: reduce(float.__add__,x),zip(*[i[1:] for i in res]))]
    average = get_average(res)
    # to get the display position of the little guy:
    # 18px (padding) + (% of distance walked so far) * 750px (line width)
    ret = [(s[1], int(s[4]*750/100+18)) for s in [first, me, average]]
    return ret
    
def projections(res, idx, days, remaining, goal):
    """What if we go past the endpoint????
    FIXME EXPLAIN
    """
    me = res[idx]
    first = res[0]
    # current pace * total_days
    projected = (days+remaining)*me[2]
    if projected > goal.total_distance:
        #projected = goal.total_distance
        days_to_finish = goal.total_distance/me[2]
        projected_date = goal.start_date + datetime.timedelta(days_to_finish)
        projected_str = "Arriving on %s" % projected_date
    else:
        projected_str = str(projected)
    first_projected = (days+remaining)*first[2]
    if first_projected > goal.total_distance:
        first_days_to_finish = goal.total_distance/first[2]
        first_projected_date = goal.start_date + datetime.timedelta(first_days_to_finish)
        first_projected_str = "Arriving on %s" % first_projected_date
    else:
        first_projected_str = str(first_projected)
    # distance_to_cover[= projected(1st)-projected(me)]/(remaining days)
    # if the event hasn't finished and you still have at least 1 day to catch the leader...
    if remaining and (me != first):
        if first_projected > goal.total_distance:
            if first_days_to_finish-days:
                first_projected = goal.total_distance
                winning_pace = (first_projected-me[1])/(first_days_to_finish-days)+1
            else:
                # There's no way we're gonna catch him
                winning_pace = None
        else:
            winning_pace = (first_projected-me[1])/remaining+1
    else:
        winning_pace = None
    return {'distance': me[1], 'perday': me[2], 'first_distance': first[1], 'projected': projected_str, 'first_projected': first_projected_str,'winning_pace':winning_pace}

def by_activity(res, idx=None):
    if not idx is None:
        query = db.GqlQuery("SELECT * FROM StatsByActivity WHERE user=:1", res[idx][0])
    else:
        query = db.GqlQuery("SELECT * FROM GlobalStatsByActivity")
    global_distance = 0
    stats = {}
    for step in query:
        stats[step.activity] = stats.get(step.activity, 0) + step.distance
        global_distance += step.distance
    res_by_activity = [(total, (act and act.name or StepConverter.COUNTER_DESCRIPTION, total, total*100.0/global_distance)) for act, total in stats.items()]
    res_by_activity.sort(reverse=True)
    return [stats for total, stats in res_by_activity]

def daily_stats(res, idx, today, start_date):
    user = res[idx][0]
    key = "daily_cumulative_%s_%s" % (user.username, today)
    daily_stats = memcache.get(key)
    if daily_stats is None:
        days = (today-start_date).days+1
        query = db.GqlQuery("SELECT * FROM DailyStats WHERE user=:1 AND date <=:2 ORDER BY date ASC", user, today)
        daily_stats = query.fetch(days)
        memcache.add(key, daily_stats, 3600)
    return daily_stats

def daily_cumulative(res, idx, today, start_date):
    """Returns an array, indexed by day number from the start of the event, with the 
    cumulative distance"""
    daily_stats = self.daily_stats(res, idx, today, start_date)
    stats = [ds.cumulative_distance for ds in daily_stats]
    return stats
    
def daily_cumulative_avg(res, today, start_date):
    query = db.GqlQuery("SELECT * FROM DailyStats WHERE date <=:1 ORDER BY date ASC", today)
    stats = [0]*((today-start_date).days+1)
    for ds in query:
        day = (ds.date-start_date).days
        stats[day] += ds.cumulative_distance
    return [s/len(res) for s in stats]
    
def daily_distances(res, idx, today, start_date):
    daily_stats = self.daily_stats(res, idx, today, start_date)
    stats = [ds.daily_distance for ds in daily_stats]
    return stats
    
def daily_distances_avg(res, today, start_date):
    query = db.GqlQuery("SELECT * FROM DailyStats WHERE date <=:1 ORDER BY date ASC", today)
    stats = [0]*((today-start_date).days+1)
    for ds in query:
        day = (ds.date-start_date).days
        stats[day] += ds.daily_distance
    return [s/len(res) for s in stats]

def populate_stats(goal, date=None):
    stats = {}
    stats_by_activity = {}
    if date:
        start_date = date
        end_date = date
        query_date = date - datetime.timedelta(1)
        query = db.GqlQuery("SELECT * FROM DailyStats WHERE date = :1 ORDER by user ", query_date)
        # Why the loop and not a 1000 fetch
        while True:
            res_lot = query.fetch(100)
            if not res_lot: break
            for res in res_lot:
                stats[res.user.key()] = (res.daily_distance,res.cumulative_distance)
        query = db.GqlQuery("SELECT * FROM StatsByActivity ORDER by user ")
        while True:
            res_lot = query.fetch(100)
            if not res_lot: break
            for res in res_lot:
                stats_by_activity[(res.user.key(),res.activity)] = res.distance
    else:
        start_date = goal.start_date
        end_date = goal.end_date

    days = (end_date-start_date).days
    total_distance = goal.total_distance

    for delta in range(days+1):
        query_date = start_date + datetime.timedelta(delta)
        query = db.GqlQuery("SELECT * FROM Step WHERE date = :1 ORDER by user ", query_date)
        current_user = None
        # FIXME We can only get 1000 anyway... so this fill stats is crap. We should do it user by user
        res_lot = query.fetch(1000)
        if not res_lot: break
        for step in res_lot:
            if step.user.key() != current_user:
                current_user = step.user.key()
                if stats.has_key(current_user):
                    stats[current_user] = (0,stats[current_user][1])
                else:
                    stats[current_user] = (0,0)
            distancia_antiga = stats[current_user]
            if (distancia_antiga[1]+step.distance > total_distance):
                stats[current_user] = (distancia_antiga[0]+step.distance, total_distance)
            else:
                stats[current_user] = (distancia_antiga[0]+step.distance, distancia_antiga[1]+step.distance)
            stats_by_activity[(current_user,step.activity)] = step.distance + stats_by_activity.get((current_user,step.activity),0)
        ds_list = [DailyStats(user=user, date=query_date, daily_distance=total[0], cumulative_distance=total[1]) for user, total in stats.items()]
        db.put(ds_list)
    if date:
        sa_query = StatsByActivity.all()
        db.delete(sa_query.fetch(1000))
    sa_list = [StatsByActivity(user=idx[0],activity=idx[1],distance=total) for idx, total in stats_by_activity.items()]
    db.put(sa_list)
    return
    
def update_stats(goal, user):
    """When a user adds some data, all stats for that user will be recalculated to make sure everything's OK
    An entry changes cummulatives for that day and all days after that. This way, we also assure that each
    user has valid statistics for all days, right from the time she makes her first input.
    
    By activity:
        - add to (user, activity) . easy, uh? :)
    but no, we'll recalculate from scratch :-(
    """
    start_date = goal.start_date
    end_date = goal.end_date
    days = (end_date-start_date).days
    total_distance = goal.total_distance
    stats = (0.0,0.0)
    stats_by_activity = {}
    ds_list = []
    for delta in range(days+1):
        stats=(0.0, stats[1])
        query_date = start_date + datetime.timedelta(delta)
        query = db.GqlQuery("SELECT * FROM Step WHERE date = :1 AND user = :2", query_date, user)
        for step in query:
            act_key = step.activity.key() if step.activity else 0
            stats = (stats[0]+step.distance, stats[1]+step.distance)
            stats_by_activity[act_key] = step.distance + stats_by_activity.get(act_key,0)
        ds_query = db.GqlQuery("SELECT * FROM DailyStats WHERE date = :1 AND user = :2", query_date, user)
        # If we're past the finish line, report as finished
        if (stats[1] > total_distance):
            stats = (stats[0], total_distance)
        # if a stats entry already exists for a day, it will be updated. Otherwise, a record will be created.
        if ds_query.count() == 0:
            ds = DailyStats(user=user, date=query_date, daily_distance=stats[0], cumulative_distance=stats[1])
        else:
            ds = ds_query.get()
            ds.daily_distance = stats[0]
            ds.cumulative_distance = stats[1]
        ds_list.append(ds)
    db.put(ds_list)
    query = db.GqlQuery("SELECT * FROM StatsByActivity WHERE user = :1", user)
    sba_del_list = []
    sba_list = []
    for sba in query:
        act = sba.activity.key() if sba.activity else 0
        if stats_by_activity.has_key(act):
            sba.distance = stats_by_activity[act]
            sba_list.append(sba)
            del stats_by_activity[act]
        else:
            sba_del_list.append(sba)
    db.delete(sba_del_list)
    db.put(sba_list)
    sa_list = [StatsByActivity(user=user,activity=act if act else None,distance=total) for act, total in stats_by_activity.items()]
    db.put(sa_list)
    query = db.GqlQuery("SELECT * FROM StatsByActivity")
    global_by_activity = {}
    for step in query:
        act_key = step.activity.key() if step.activity else 0
        global_by_activity[act_key] = global_by_activity.get(act_key, 0) + step.distance
    query = db.GqlQuery("SELECT * FROM GlobalStatsByActivity")
    gsba_del_list = []
    gsba_list = []
    for gsba in query:
        act = gsba.activity.key() if gsba.activity else 0
        if global_by_activity.has_key(act):
            gsba.distance = global_by_activity[act]
            gsba_list.append(gsba)
            del global_by_activity[act]
        else:
            gsba_del_list.append(gsba)
    db.delete(gsba_del_list)
    db.put(gsba_list)
    # new ones
    gsa_list = [GlobalStatsByActivity(activity=act if act else None,distance=total) for act, total in global_by_activity.items()]
    db.put(gsa_list)
    return
    
def clear_stats(self):
    """ NOTE - This has to be called several times over multiple requests if you have more
    than 1000 entries for any of these 3 kinds"""
    remove_by_query(DailyStats.all())
    remove_by_query(StatsByActivity.all())
    remove_by_query(GlobalStatsByActivity.all())
    return
    
def crop_stats(goal):
    """Deletes any stats outside of our goal dates. Should be followed by calling update_stats
    for all the users.
    
    FIXME fails to delete all if any query returns more than 1000 results
    """
    start_date = goal.start_date
    end_date = goal.end_date
    query = db.GqlQuery("SELECT * FROM Step WHERE date < :1", start_date)
    remove_by_query(query)
    query = db.GqlQuery("SELECT * FROM Step WHERE date > :1", end_date)
    remove_by_query(query)
    query = db.GqlQuery("SELECT * FROM DailyStats WHERE date < :1", start_date)
    remove_by_query(query)
    query = db.GqlQuery("SELECT * FROM DailyStats WHERE date > :1", end_date)
    remove_by_query(query)
