from google.appengine.api import memcache
from walkon.views import WALKON_CONFIG_NAME

def goal(request):
    from walkon.models import Goal, GlobalConfig
    config = GlobalConfig.get_or_insert(WALKON_CONFIG_NAME)
    goal = memcache.get("goal")
    if goal is None:
        from walkon.models import Goal
        goal_query = Goal.all()
        goal = goal_query.get()
        memcache.add("goal", goal, 3600)
    return {'goal': goal, 'global_config': config}
