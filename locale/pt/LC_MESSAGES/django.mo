��          �   %   �      `  
   a     l     �     �     �     �     �     �     �     �  $   �                 	   !     +     D  �   `     �     �            /         P  x   m     �  5  �     !  !   -     O  
   d     o     �  	   �     �     �  	   �  &   �     �     �  	   �  	         
     !  �   4     �     �     �     �  :   �     6  s   V     �                                                            	                       
                                         Activities Add a new activity to the list Add activity Avatar Change avatar Change image Congratulations Create Description Goal If you have an account you can login Links Login required Main Objective Or you can join the site Please define the objective Please take care when changing the objective after the starting date - if users have entered any data it may cause inconsistencies Steps/Minute The competiton has started Theme Theme Settings You need to be logged to perform this operation Your changes have been saved Your registration is complete and you are now logged in the site. <a href="/">Click here</a> to return to the main page. here Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2009-03-29 23:37+0100
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Actividades Adicionar uma actividade à lista Adicionar actividade Fotografia Alterar fotografia Alterar imagem Parabéns Criar Descrição Objectivo Se tem uma conta pode efectuar o login Links Login necessário Principal Objective Ou pode aderir ao site Defina o objectivo Atenção que alterar o objectivo depois do início pode originar inconsistências com os resultados inseridos pelos participantes Passos/Minuto A competição já começou Tema Definições do Tema Precisa de estar autenticado para efectuar esta operação As alterações foram guardadas O registo está completo e o login foi efectuado. <a href="/">Clique aqui</a> para voltar para a página principal. aqui 