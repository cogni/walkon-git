from appengine_django import InstallAppengineHelperForDjango
InstallAppengineHelperForDjango()

from google.appengine.ext import bulkload
from google.appengine.api import datastore_types
from google.appengine.ext import search
from google.appengine.ext import db
import datetime
import walkon.models

class StepLoader(bulkload.Loader):
  def __init__(self):
    bulkload.Loader.__init__(self, 'Step',
                    [('user', lambda x: db.Key(x)),
                     ('steps', int),
                     ('distance', float),
                     ('date', lambda x: datetime.datetime.strptime(x, '%Y-%m-%d')),
                     ('description', str),
                     ])

  def HandleEntity(self, entity):
    # for full-text search
    #ent = search.SearchableEntity(entity)
    #return ent
    return entity

if __name__ == '__main__':
  bulkload.main(StepLoader())