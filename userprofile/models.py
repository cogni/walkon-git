import re
import sha
import random
from datetime import datetime, timedelta
from google.appengine.ext import db
from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _
import settings
from userprofile import utils
from ragendja.auth.models import User as RjUser

def init_avatar():
    from google.appengine.api.urlfetch import fetch
    d = fetch('%s/img/default_avatar_large.jpg' % settings.DOMAIN_URL)
    avatar = Avatar(image=d.content, status='public',filename='default_avatar_large.jpg')
    avatar.put()
    return avatar

class Theme(db.Model):
    name = db.StringProperty()
    bg_image_path = db.StringProperty()
    thumbnail_path = db.StringProperty()
    bg_color = db.StringProperty()
    bg_repeat = db.BooleanProperty()
    site_color1 = db.StringProperty()
    site_color2 = db.StringProperty()
    link_color = db.StringProperty()
    
    def get_bg_url(self):
        return self.bg_image_path
        
class Avatar(db.Model):
    status = db.StringProperty(choices=set(['public','private']), default='private')
    image = db.BlobProperty(default=None)
    filename = db.StringProperty()

    @classmethod
    def get_default_avatar(cls):
        avatar = Avatar.all().filter('status =','public').get()
        if not avatar:
            avatar = init_avatar()
        return avatar

class User(RjUser):
    display_name = db.StringProperty()
    bio = db.TextProperty()
    avatar = db.Reference(Avatar)
    location = db.StringProperty()
    theme = db.Reference(Theme)
    bg_name = db.StringProperty()
    bg_color = db.StringProperty()
    bg_repeat = db.BooleanProperty()
    site_color1 = db.StringProperty()
    site_color2 = db.StringProperty()
    link_color = db.StringProperty()
    
    def __init__(self, *args, **kwargs):
        super(User, self).__init__(*args, **kwargs)
        from walkon.models import GlobalConfig
        self.GCONFIG = GlobalConfig.all().get()

    def get_absolute_url(self):
        return reverse('userpage', kwargs={'username': self.username})
    
    def is_anonymous(self):
        "Always returns False. This is a way of comparing User objects to anonymous users."
        return False

    def get_display_name(self):
        return self.display_name or self.username
    
    def get_full_name(self):
        return self.get_display_name()
            
    def get_bgimage(self):
        "return the user, theme or default background image"
        if self.bg_name:
            return reverse('theme_image', kw_args={'userkey': self.key, 'filename':self.bg_name})
        elif self.theme:
            return self.theme.bg_image_path
        elif self.GCONFIG.theme:
            return self.GCONFIG.theme.bg_image_path
        elif self.GCONFIG.bg_name:
            return reverse('global_theme_image', kw_args={'filename':self.GCONFIG.bg_name})
        else:
            return None

    def get_bgcolor(self):
        "returns the user background color or the default background color"
        return self.get_theme_field('bg_color')
    
    def get_bgrepeat(self):
        return self.get_theme_field('bg_repeat')

    def get_linkcolor(self):
        return self.get_theme_field('link_color')

    def get_sitecolor1(self):
        return self.get_theme_field('site_color1')

    def get_sitecolor2(self):
        return self.get_theme_field('site_color2')
         
    def get_theme_field(self, fieldname):
        if getattr(self, fieldname):
            return getattr(self, fieldname)
        elif self.theme:
            return getattr(self.theme, fieldname)
        elif getattr(self.GCONFIG, fieldname):
            return getattr(self.GCONFIG, fieldname)
        elif self.GCONFIG.theme:
            return getattr(self.GCONFIG.theme, fieldname)
        else:
            return None
    
    def put(self):
        if not self.avatar:
            self.avatar = Avatar.get_default_avatar()
        super(User,self).put()


class BgImage(db.Model):
    """
    Background image for user theme customization 
    """
    user = db.Reference(User, required=True)
    filename = db.StringProperty()
    image = db.BlobProperty(default=None)
    
    
class Link(db.Model):
    """
    Links for user profile page
    """
    user = db.Reference(User, required=True)
    name = db.StringProperty(required=True)
    url = db.LinkProperty(required=True)
    logo = db.StringProperty()
    
    def put(self):
        self.logo = utils.find_link_logo(self.url)
        super(Link,self).put()

#From appenginepatch sample app

SHA1_RE = re.compile('^[a-f0-9]{40}$')

class RegistrationManager(models.Manager):

    def activate_user(self, activation_key):
        """
        Validate an activation key and activate the corresponding
        ``User`` if valid.        
        """
        # Make sure the key we're trying conforms to the pattern of a
        # SHA1 hash; if it doesn't, no point trying to look it up in
        # the database.
        if SHA1_RE.search(activation_key):
            profile = RegistrationProfile.get_by_key_name("key_"+activation_key)
            if not profile:
                return False
            if not profile.activation_key_expired():
                user = profile.user
                user.is_active = True
                user.put()
                profile.activation_key = RegistrationProfile.ACTIVATED
                profile.put()
                return user
        return False
    
    def create_inactive_user(self, username, password, email, domain_override="", 
                             send_email=True, from_email=None):
        """
        Create a new, inactive ``User``, generate a
        ``RegistrationProfile`` and email its activation key to the
        ``User``, returning the new ``User``.        
        """
        # prepend "key_" to the key_name, because key_names can't start with numbers
        new_user = User(username=username, key_name="key_"+username.lower(),
            email=email, is_active=False)
        new_user.set_password(password)
        new_user.put()
        
        registration_profile = self.create_profile(new_user)
        
        if send_email:
            from django.core.mail import send_mail
            current_site = domain_override
            subject = _("Activation email for ") + current_site
            message = render_to_string('userprofile/activation_email.txt',
                                       { 'activation_key': registration_profile.activation_key,
                                         'expiration_days': settings.ACCOUNT_ACTIVATION_DAYS,
                                         'site': current_site })
            if not from_email:
                from_email = settings.DEFAULT_FROM_EMAIL
            send_mail(subject, message, from_email, [new_user.email])
        return new_user
    
    def create_profile(self, user):
        """
        The activation key for the ``RegistrationProfile`` will be a
        SHA1 hash, generated from a combination of the ``User``'s
        username and a random salt.        
        """
        salt = sha.new(str(random.random())).hexdigest()[:5]
        activation_key = sha.new(salt+user.username).hexdigest()
#       prepend "key_" to the key_name, because key_names can't start with numbers     
        registrationprofile = RegistrationProfile(user=user, activation_key=activation_key, key_name="key_"+activation_key)
        registrationprofile.put()
        self.delete_expired_users()
        return registrationprofile
        
    def delete_expired_users(self):
        for profile in RegistrationProfile.all():
            if profile.activation_key_expired():
                user = profile.user
                if not user.is_active:
                    user.delete()
                    profile.delete()

class RegistrationProfile(db.Model):
    """ 
    Stores an activation key for use during
    user account registration 
    """
    ACTIVATED = u"ALREADY_ACTIVATED"
    
    user = db.ReferenceProperty(User, verbose_name=_('user'))
    activation_key = db.StringProperty(_('activation key'))
    objects = RegistrationManager()
    
    def __unicode__(self):
        return u"Registration information for %s" % self.user
    
    def activation_key_expired(self):
        """
        Determine whether this ``RegistrationProfile``'s activation
        key has expired, returning a boolean -- ``True`` if the key
        has expired.
        
        Key expiration is determined by a two-step process:
        
        1. If the user has already activated, the key will have been
           reset to the string constant ``ACTIVATED``. Re-activating
           is not permitted, and so this method returns ``True`` in
           this case.

        2. Otherwise, the date the user signed up is incremented by
           the number of days specified in the setting
           ``ACCOUNT_ACTIVATION_DAYS`` (which should be the number of
           days after signup during which a user is allowed to
           activate their account); if the result is less than or
           equal to the current date, the key has expired and this
           method returns ``True``.
        
        """
        expiration_date = timedelta(days=settings.ACCOUNT_ACTIVATION_DAYS)
        return self.activation_key == RegistrationProfile.ACTIVATED or \
               (self.user.date_joined + expiration_date <= datetime.now())
    activation_key_expired.boolean = True