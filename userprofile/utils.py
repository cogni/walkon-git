import urlparse
    
LOGO_MAP = (
    ('del.icio.us','delicious'),
    ('digg.com','digg'),
    ('facebook.com','facebook'),
    ('flickr.com','flickr'),
    ('hi5.com','hi5'),
    ('jaiku.com','jaiku'),
    ('last.fm','lastfm'),
    ('linkedin.com', 'linkedin'),
    ('orkut.com','orkut'),
    ('twitter.com','twitter'),
    ('youtube.com','youtube'),
)

def site_name(url):
    "gets the site name from the url"
    url_parsed = urlparse.urlparse(url)
    return url_parsed.hostname

def find_link_logo(url):
    for pat, logo in LOGO_MAP:
        site = site_name(url)
        if site.endswith(pat):
            return logo
    return 'generic' 
