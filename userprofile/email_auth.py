# -*- coding: utf-8 -*-
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import User
import re

"""
Based on the code of satchmo 
http://www.satchmoproject.com/trac/browser/satchmo/trunk/satchmo/apps/satchmo_store/accounts/email-auth.py

In setttings.py use
 
AUTHENTICATION_BACKENDS = (
    'userprofile.email_auth.EmailBackend',
)
"""

email_re = re.compile(
            r"(^[-!#$%&'*+/=?^_`{}|~0-9A-Z]+(\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*"  # dot-atom
            r'|^"([\001-\010\013\014\016-\037!#-\[\]-\177]|\\[\001-\011\013\014\016-\177])*"' # quoted-string
            r')@(?:[A-Z0-9-]+\.)+[A-Z]{2,6}$', re.IGNORECASE)  # domain

            
class EmailBackend(ModelBackend):
    """Authenticate using user or email"""
    def authenticate(self, username=None, password=None):
        #If username is an email address, then try to pull it up
        if email_re.search(username):
            user = User.all().filter('email =', username).get()
        else:
            user = User.all().filter('username =', username).get()    
        if user and user.check_password(password):
            return user
        else:
            return None