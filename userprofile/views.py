# -*- coding: utf-8 -*-
import re
import logging
import mimetypes
from datetime import datetime, date, timedelta
from google.appengine.ext import db
from django.conf import settings 
from django.contrib.sites.models import Site, RequestSite
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout, REDIRECT_FIELD_NAME
from django.contrib.auth.views import password_reset
from django.core.paginator import QuerySetPaginator
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render_to_response
from django.template import RequestContext, Context, loader
from django.utils.http import urlquote_plus
from django.utils.translation import ugettext as _
from django.views.decorators.cache import never_cache
from userprofile.models import Avatar, Link, BgImage, RegistrationProfile, Theme
from userprofile.forms import ProfileForm, AvatarForm, ProfileLinkForm, RegisterForm, \
                        PasswordResetForm, UserAdminForm, ThemeForm, EmailAuthenticationForm
from cogni.shortcuts import getkey_object_or_404
from walkon.models import GlobalConfig

PAGE_SIZE = 100

def load_user(request):
    from userprofile import initial_load
    initial_load.load()
    return HttpResponse("ok")

def do_logout(request):
	logout(request)			
	return HttpResponseRedirect('/')

def register(request):
    ctx = {}
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            username, password, email = ( form.cleaned_data['username'],
                        form.cleaned_data['password'], form.cleaned_data['email'])
            # prepend "key_" to the key_name, because key_names can't start with numbers
            user = User(key_name="key_%s" % username, username=username, email=email)
            user.set_password(password)
            user.is_active = True
            user.put()
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
            if request.session.test_cookie_worked():
                request.session.delete_test_cookie()
            if request.GET.has_key('next'):
                return HttpResponseRedirect(request.GET['next'])
            else:
                return HttpResponseRedirect('/')
    else:
        form = RegisterForm()
    ctx['form'] = form
    request.session.set_test_cookie()
    if request.GET.has_key('next'):
        ctx['next'] = request.GET['next']
    return render_to_response('userprofile/register.html', ctx, RequestContext(request))


def register_inactive_user(request):
    """ Register inactive user, needs email confirmation to be active """
    
    ctx = {}
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            globalconf = GlobalConfig.all().get()
            if globalconf and globalconf.from_email:
                from_email = globalconf.from_email
            else:
                from_email = settings.DEFAULT_FROM_EMAIL
            new_user = RegistrationProfile.objects.create_inactive_user(username=form.cleaned_data['username'],
                            password=form.cleaned_data['password'], email=form.cleaned_data['email'],
                            domain_override=request.get_host(), from_email=from_email)
            return HttpResponseRedirect(reverse('register_complete'))
    else:
        form = RegisterForm()
    ctx['form'] = form
    request.session.set_test_cookie()
    if request.GET.has_key('next'):
        ctx['next'] = request.GET['next']
    return render_to_response('userprofile/register.html', ctx, RequestContext(request))


def activate(request, activation_key, template_name='userprofile/activate.html', extra_context=None):
    """
    Activate a ``User``'s account from an activation key, if their key
    is valid and hasn't expired.
    """
    activation_key = activation_key.lower() # Normalize before trying anything with it.
    account = RegistrationProfile.objects.activate_user(activation_key)
    if account:
        return HttpResponseRedirect(reverse('register_activate_complete'))
    if extra_context is None:
        extra_context = {}
    context = RequestContext(request)
    for key, value in extra_context.items():
        context[key] = callable(value) and value() or value
    return render_to_response(template_name, { 'account': account, 'expiration_days': settings.ACCOUNT_ACTIVATION_DAYS },
                              context_instance=context)

def activate_complete(request):
    return render_to_response('userprofile/activate_complete.html', { }, context_instance=RequestContext(request))

def do_password_reset(request):
    return password_reset(request, template_name='userprofile/getpassword.html', 
                email_template_name='userprofile/password_reset_email.html', password_reset_form=PasswordResetForm)


def avatar_image(request, userkey, filename):
    user = User.get(db.Key(userkey))
    if user and user.avatar:
        mt = mimetypes.guess_type(user.avatar.filename)[0]
        return HttpResponse(user.avatar.image, mimetype=mt)
    else:
        return HttpResponse(Avatar.get_default_avatar().image, mimetype='image/jpeg')

def theme_image(request, userkey, filename):
    user = User.get(db.Key(userkey))
    if user and user.bg_name:
        bg_image = BgImage.all().filter('user =',user).filter('filename =',filename).get()
        if bg_image:
            mt = mimetypes.guess_type(filename)[0]
            return HttpResponse(bg_image.image, mimetype=mt)
    raise Http404

@login_required
def user_settings(request):
    msg = request.GET.get('msg','')
    initial = {}
    profile_fields = ['display_name','bio','location']
    for k in profile_fields:
        initial[k] = getattr(request.user, k)

    if request.method == 'POST':
        form = ProfileForm(request.POST)
        if form.is_valid():
            form.save(request.user)
            msg = _("Settings changed") 
            redirect = "%s?msg=%s" % (reverse('user_settings'), urlquote_plus(msg))
            return HttpResponseRedirect(redirect)
    else:
        form = ProfileForm(initial=initial)
    return render_to_response('userprofile/user_settings.html', {'form': form, 'msg': msg }, 
                RequestContext(request))
                
@login_required
def user_link(request):
    msg = request.GET.get('msg','')
    if request.method == 'POST':
        form = ProfileLinkForm(request.POST)
        if form.is_valid():
            form.save(request.user)
            return HttpResponseRedirect("%s?msg=%s" % (reverse('user_link'), _("Link added")))
    else:
        form = ProfileLinkForm()
    return render_to_response('userprofile/user_link.html', { 'form':form, 'msg':msg },
                              RequestContext(request))
                              
                              
# FIXME this should use a POST form and not a link
@login_required
def link_remove(request, link_key):
    link = getkey_object_or_404(Link, link_key)
    if link.user.key() == request.user.key():
        link.delete()
    return HttpResponseRedirect('%s?msg=%s' % (reverse('user_link'), _("Link removed")))

@login_required
def user_avatar(request):
    msg = request.GET.get('msg','')
    if request.method == 'POST':
        form = AvatarForm(request.POST, request.FILES)
        if form.is_valid():
            request.user.avatar = form.save()
            request.user.put()
            return HttpResponseRedirect('%s?msg=%s' % (reverse('user_avatar'), _("Avatar changed")))
    else:
        form = AvatarForm()
    return render_to_response('userprofile/avatar.html', { 'form':form, 'msg':msg },
                              RequestContext(request))

@login_required
def user_admin(request):
    if request.user.is_staff:
        page_nr = request.REQUEST.get('page', 1)
        users = User.all().order('-date_joined')
        paginator = QuerySetPaginator(users, PAGE_SIZE)
        page = paginator.page(page_nr)
        ctx = { 'users': users, 'page': page }
        return render_to_response('userprofile/user_admin.html', ctx, RequestContext(request))
    else:
        raise Http404

@login_required
def user_admin_detail(request, username):
    if request.user.is_staff:
        user = User.all().filter('username =', username).get()
        if user:
            if request.method == 'POST':
                form = UserAdminForm(request.POST)
                if form.is_valid():
                    form.save(user)
                    return HttpResponseRedirect(reverse('user_admin'))
            else:
                form = UserAdminForm(initial={'karma': user.karma, 'is_staff': user.is_staff})
            ctx = {'user': user, 'form': form}
            return render_to_response('userprofile/user_admin_detail.html', ctx, RequestContext(request))
        else:
            raise Http404
    else:
        raise Http404

@login_required
def user_theme(request):
    user = request.user
    msg = request.GET.get('msg','')
    if request.method == 'POST':
        form = ThemeForm(request.POST, request.FILES)
        if form.is_valid():
            form.save(user)
            return HttpResponseRedirect('%s?msg=%s' % (reverse('user_theme'),_("Theme changed")))
    else:
        thm = request.GET.get('theme','')
        if thm == 'default':
            tm = None
        elif thm:
            tm = Theme.all().filter('name =', thm).get()
        else:
            tm = None
        if thm:
            user.theme = tm
            user.bg_repeat = False
            user.bg_color = user.filename = user.bg_color = ''
            user.bg_image = None
            user.link_color = user.site_color1 = user.site_color2 = ''
            user.save()
            msg = _("Theme changed")
        form = ThemeForm(initial={'bg_color':user.bg_color, 'bg_repeat':user.bg_repeat, 'filename': user.bg_name, 'path': reverse('theme_image', kwargs={'userkey': user.key(), 'filename': ''})})
    return render_to_response('userprofile/theme.html', { 'form':form, 'msg':msg, 'themes': Theme.all() },
                                  RequestContext(request))

#modified from django auth to allow email login (needed because the Authentication form is not configurable)
def email_login(request, template_name='registration/login.html', 
                auth_form=EmailAuthenticationForm, redirect_field_name=REDIRECT_FIELD_NAME):
    "Displays the login form and handles the login action."
    redirect_to = request.REQUEST.get(redirect_field_name, '')
    if request.method == "POST":
        form = auth_form(data=request.POST)
        if form.is_valid():
            # Light security check -- make sure redirect_to isn't garbage.
            if not redirect_to or '//' in redirect_to or ' ' in redirect_to:
                redirect_to = settings.LOGIN_REDIRECT_URL
            from django.contrib.auth import login
            login(request, form.get_user())
            if request.session.test_cookie_worked():
                request.session.delete_test_cookie()
            return HttpResponseRedirect(redirect_to)
    else:
        form = auth_form(request)
    request.session.set_test_cookie()
    if Site._meta.installed:
        current_site = Site.objects.get_current()
    else:
        current_site = RequestSite(request)
    return render_to_response(template_name, {
        'form': form,
        redirect_field_name: redirect_to,
        'site_name': current_site.name,
    }, context_instance=RequestContext(request))
email_login = never_cache(email_login)