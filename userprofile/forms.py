# -*- coding: utf-8 -*-
import re
import logging
import string
from django import forms
from django.conf import settings
from django.template import Context, loader
from django.contrib.auth.models import User
from django.utils.encoding import smart_str
from django.utils.html import strip_tags
from django.contrib.sites.models import Site
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.tokens import default_token_generator
from django.utils.translation import ugettext as _
from django.contrib.auth.models import User
from userprofile.models import Avatar, Link, BgImage
from walkon.removable_file_field import RemovableFileFormField

alnum_re = re.compile(r'^\w+$')
username_re = r"^[A-Za-z0-9-_]{3,20}$"

RESERVED_WORDS = ['admin', 'sysadmin']
                  
class CustomForm(forms.Form):
    def as_table(self):
        "Returns this form rendered as HTML <tr>s -- excluding the <table></table>."
        return self._html_output(u'<tr><th>%(label)s</th><td>%(errors)s%(field)s</td><td class="info">%(help_text)s</td></tr>', u'<tr><th></th><td colspan="2" class="errorfield">%s</td></tr>', '</td></tr>', u'%s', False)


class ProfileForm(forms.Form):
    display_name = forms.CharField(label=_("Your name"), required=False, 
                        widget=forms.TextInput(attrs={'class':'textfield'}))    
    bio = forms.CharField(required=False, label=_("Short bio"),
                        widget=forms.Textarea(attrs={'class':'textarea'}))
                        
    def save(self, user):
         user.display_name = strip_tags(self.cleaned_data['display_name'])
         user.bio = strip_tags(self.cleaned_data['bio'])
         user.put()


class AvatarForm(forms.Form):
    image = forms.FileField(label=_("Image file"))

    def clean_image(self):
        if hasattr(settings, 'MAX_UPLOAD_SIZE') and self.cleaned_data['image'].size > settings.MAX_UPLOAD_SIZE:
            max_kbytes = int(settings.MAX_UPLOAD_SIZE / 1024)
            raise forms.ValidationError(_("Please use a file with size under %s kb.") % max_kbytes)
        return self.cleaned_data['image']
    
    def save(self):
        image = self.cleaned_data['image']
        avatar = Avatar(image=image.read(), filename=image.name)
        avatar.put()
        return avatar


class ThemeForm(forms.Form):
    bg_color = forms.CharField(label='Background color', required=False)
    bg_image = RemovableFileFormField(label='Background image', required=False,
                help_text=_("Maximum file size of %s kb" % int(settings.MAX_UPLOAD_SIZE / 1024)))
    bg_repeat = forms.BooleanField(label='Image repeat', required=False)
    link_color = forms.CharField(label='Link color', required=False)
    site_color1 = forms.CharField(label='Site color 1', required=False)
    site_color2 = forms.CharField(label='Site color 2', required=False)

    def clean_bg_image(self):
        newfile = self.cleaned_data['bg_image'][0]
        if newfile and hasattr(settings, 'MAX_UPLOAD_SIZE') and newfile.size > settings.MAX_UPLOAD_SIZE:
            max_kbytes = int(settings.MAX_UPLOAD_SIZE / 1024)
            raise forms.ValidationError(_("Please use a file with size under %s kb.") % max_kbytes)
        return self.cleaned_data['bg_image']

    def save(self, user):
        if self.cleaned_data['bg_image'] and self.cleaned_data['bg_image'][0]:
            filename = '%s_%s' % (user.username, self.cleaned_data['bg_image'][0].name)
            bg_image = BgImage.all().filter('user =', user).get()
            if not bg_image:
                bg_image = BgImage(image=self.cleaned_data['bg_image'][0].read(), filename=filename, user=user)
            else:
                bg_image.image = self.cleaned_data['bg_image'].read()
                bg_image.filename = filename
            bg_image.save()
            user.bg_name = filename
        elif self.cleaned_data['bg_image'] and self.cleaned_data['bg_image'][1]:
            bg_image = BgImage.all().filter('user =', user).get()
            if bg_image:
                bg_image.delete()
            user.bg_name = ''
        #else:
            #user.bg_name = ''
        user.bg_repeat = self.cleaned_data['bg_repeat']
        user.bg_color = self.cleaned_data['bg_color']
        user.link_color = self.cleaned_data['link_color']
        user.site_color1 = self.cleaned_data['site_color1']
        user.site_color2 = self.cleaned_data['site_color2']
        user.theme = None
        user.save()

    def __init__(self, *args, **kwargs):
        filename = ''
        if kwargs.has_key('initial') and kwargs['initial'].has_key('filename'):
            filename = kwargs['initial']['filename']
        path = ''
        if kwargs.has_key('initial') and kwargs['initial'].has_key('path'):
            path = kwargs['initial']['path']
        super(ThemeForm, self).__init__(*args, **kwargs)
        self.fields['bg_image'].widget.set_filename(filename)
        self.fields['bg_image'].widget.set_path(path)

    
class ProfileLinkForm(forms.Form):
    name = forms.CharField(label=_("Link name"), widget=forms.TextInput(attrs={'class':'textfield'}),
                help_text=_("Link description. Examples: blog, Hi5 profile, myspace, etc"))
    url = forms.CharField(label=_("URL"), widget=forms.TextInput(attrs={'class':'textfield'}),
                help_text=_("Complete URL. Example: http://example.com"))
                
    def save(self, user):
        link = Link(user=user, name=self.cleaned_data['name'], url=self.cleaned_data['url'])
        link.put()
        return link


class RegisterForm(forms.Form):
    username = forms.CharField(label=_("Username"), widget=forms.TextInput(attrs={'class':'textfield'}))
    password = forms.CharField(label=_("Password"), widget=forms.PasswordInput(attrs={'class':'textfield'}))
    password2 = forms.CharField(label=_("Confirm password"), widget=forms.PasswordInput(attrs={'class':'textfield'}))
    email = forms.EmailField(label=_("Email"), widget=forms.TextInput(attrs={'class':'textfield'}),
            help_text=_("Email will only be used for password reset"))
    
    def clean_username(self):
        username = self.cleaned_data.get('username',None)
        if not username:
            raise forms.ValidationError(_("Invalid username"))
        if re.match(username_re, username):
            if username in RESERVED_WORDS:
                raise forms.ValidationError(_('Username "%s" cannot be used. Please choose other.' % username))
            
            user = User.all().filter('username =', username).get()
            if user:
                raise forms.ValidationError(_('Username "%s" already being used. Please choose other.' % username))
            else:
                return username
        else:
            raise forms.ValidationError(_("Invalid username"))

    def clean_password2(self):
        if self.cleaned_data.get('password', None) and self.cleaned_data.get('password2', None) and \
           self.cleaned_data['password'] == self.cleaned_data['password2']:
            return self.cleaned_data['password2']
        raise forms.ValidationError(u'Passwords diferentes')

    def clean_email(self):
        if self.cleaned_data.get('email', None):
            user = User.all().filter('email =', self.cleaned_data['email']).get()
            if user:
                raise forms.ValidationError(_("Email already being used"))
            else:
                from walkon.models import GlobalConfig
                gconf = GlobalConfig.all().get()
                if gconf and gconf.allowed_domains:
                    domains = map(string.strip, gconf.allowed_domains.split(','))
                    allowed = False
                    for domain in domains:
                        if self.cleaned_data['email'].endswith(domain):
                            allowed = True
                    if not allowed:
                        raise forms.ValidationError(_("Email domain not in the authorized domains list"))
                return self.cleaned_data['email']
        else:
            raise forms.ValidationError(_("Missing or invalid email"))


class PasswordResetForm(forms.Form):
    email = forms.EmailField(label="Email", widget=forms.TextInput(attrs={'class':'textfield'}))
    
    def clean_email(self):
        " Validates that a user exists with the given e-mail address."
        email = self.cleaned_data["email"]
        self.users_cache = User.all().filter('email =', email).fetch(100)
        if len(self.users_cache) == 0:
            raise forms.ValidationError(_("Invalid email"))
    

    def save(self, domain_override=None, email_template_name='registration/password_reset_email.html',
             use_https=False, token_generator=default_token_generator):
        """
        Generates a one-use only link for resetting password and sends to the user
        """
        from django.core.mail import send_mail
        for user in self.users_cache:
            if not domain_override:
                current_site = Site.objects.get_current()
                site_name = current_site.name
                domain = current_site.domain
            else:
                site_name = domain = domain_override
            t = loader.get_template(email_template_name)
            c = {
                'email': user.email,
                'user': user,
                'site_name': site_name,
                'domain': domain,
                'token': token_generator.make_token(user),
                'protocol': use_https and 'https' or 'http',
            }
            from walkon.models import GlobalConfig
            from walkon.views import WALKON_CONFIG_NAME
            config = GlobalConfig.get_or_insert(WALKON_CONFIG_NAME)
            from_email = config.from_email or settings.DEFAULT_FROM_EMAIL
            send_mail("WalkOn password reset",
                t.render(Context(c)), from_email, [user.email])
    
    
class UserAdminForm(CustomForm):
    karma = forms.IntegerField(label=_("Rank"), widget=forms.TextInput(attrs={'class':'textfield'}),
                help_text=_("Rank for the user (0 - new user, 1 - regular user, 2 - trusted user)"))
    is_staff = forms.BooleanField(label=_("Staff"), required=False, 
                help_text=_("Staff users can manage users and links"))
    
    def save(self, user):
        user.karma = self.cleaned_data['karma']
        user.is_staff = self.cleaned_data['is_staff']
        user.put()


class EmailAuthenticationForm(AuthenticationForm):
    """Authentication form with a longer username field and a different label"""
    def __init__(self, *args, **kwargs):
        super(EmailAuthenticationForm, self).__init__(*args, **kwargs)
        username = self.fields['username']
        username.max_length = 75
        username.label = _("Username or email")
        username.widget.attrs['maxlength'] = 75
