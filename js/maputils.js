
function walkonIcon(icontype)
{
    var wi = new GIcon(G_DEFAULT_ICON);
    switch(icontype)
    {
        case walkonIconTypes.START_ICON:
            wi.image = "/img/markers/blue_MarkerSt.png"; break;
        case walkonIconTypes.FINISH_ICON:
            wi.image = "/img/markers/blue_MarkerCh.png"; break;
        case walkonIconTypes.LEADER_ICON:
            wi.image = "/img/markers/green_Marker1.png"; break;
        case walkonIconTypes.AVERAGE_ICON:
            wi.image = "/img/markers/yellow_MarkerA.png"; break;
        case walkonIconTypes.LAST_ICON:
            wi.image = "/img/markers/red_MarkerL.png"; break;
        case walkonIconTypes.ME_ICON:
            wi.image = "/img/markers/black_Marker.png"; break;
    }
    return wi;
}

walkonIconTypes = { START_ICON: 0, FINISH_ICON: 1, LEADER_ICON: 2, AVERAGE_ICON: 3, LAST_ICON: 4, ME_ICON: 5 }