$(function() {
//    $("#save-steps").click(function() {
//         var formdata = {};
//         formdata['date'] = $("#distance-form input[name='date']").val();
//         $("#distance-form :text").each(function(i) {
//             if ($(this).val() != '') formdata[$(this).attr('name')]= $(this).val();
//         });
//         $.post("setstepsajax", formdata);
//         window.location.reload();
//         return false;
//     });

    $("#date-form").validate({
        rules: {
            date: {
            required: true,
            dateISO: true
            }
        },
         messages: {
            date: {
            required: "This field is required.",
            dateISO: "Enter a valid date in YYYY-MM-DD format."
            }
        },
        errorPlacement: function(error, element) {
            error.append("<br/>");
            error.prependTo( element.parent("p") );
        },
        errorClass: "errorText",
        showErrors: function(errorMap, errorList) {
            if (this.numberOfInvalids() > 0)
                $("#date-form p").addClass("error");
            else
                $("#date-form p").removeClass("error");
            this.defaultShowErrors();
        },
        onkeyup: false,
        onclick: false,
        onfocusout: false
    })
 });

function get_step_table(date_str)
{
    $("#step-table thead").hide();
    $("#step-table-body-yes").hide();
    $("#step-table-body-yes").empty();
    $("#step-table-body-no").hide();
    $("#step-table").after('<div id="ajax-loader" class="ajax-loader"><img src="/img/ajax-loader.gif"/></div>');
    $.get("/getsteptable/", {'date': date_str},
        function(data){
            $("#ajax-loader").remove();
            if (data != '')
            {
                $("#step-table thead").show();
                $("#step-table-body-yes").html(data);
                $("#step-table-body-yes").show();
            }
            else
            {
                $("#step-table-body-no").show();
            }
        }
    );
}

function clear_hilite()
{
    $('.hilite').remove()
}

function change_date(date_str, tab)
{
    $('.date-hidden').val(date_str);
    $(".selected").removeClass("selected");
    tab.addClass("selected");
    $('#tab_date').text("Calendar");
    clear_hilite()
    get_step_table(date_str);
    return false;
}

function get_full_step_form(date_str)
{
    $("#step_form_container").show();
    $("#step_form_container").empty();
    $("#step_form_container").after('<div id="ajax-loader-sf" class="ajax-loader"><img src="/img/ajax-loader.gif"/></div>');
    $.get("/getfullstepform/", {'date': date_str },
        function(data){
            $("#ajax-loader-sf").remove();
            if (data != '')
            {
                $("#step_form_container").show();
                $("#step_form_container").html(data);
            }
            else
                $("##step_form_container").hide();
        }
    );
}

$(document).ready(function() {
    $('.hilite').animate({backgroundColor: 'yellow'}, 1000).animate( { backgroundColor: 'white' }, 1000);
    ;
});