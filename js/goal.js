$(function() {
    $("#id_start_date").DatePicker({
        format:'Y-m-d',
        date: $('#id_start_date').val(),
        current: $('#id_start_date').val(),
        starts: 1,
        position: 'right',
        onBeforeShow: function(){
            $('#id_start_date').DatePickerSetDate($('#id_start_date').val(), true);
        },
        onChange: function(formated, dates){
            $('#id_start_date').val(formated);
            $('#id_start_date').DatePickerHide();
        }
    });

     $("#id_end_date").DatePicker({
        format:'Y-m-d',
        date: $('#id_end_date').val(),
        current: $('#id_end_date').val(),
        starts: 1,
        position: 'right',
        onBeforeShow: function(){
            $('#id_end_date').DatePickerSetDate($('#id_end_date').val(), true);
        },
        onChange: function(formated, dates){
            $('#id_end_date').val(formated);
            $('#id_end_date').DatePickerHide();
        }
    });
    
    $("#goal-form").validate({
        rules: {
            start_date: {
            required: true,
            dateISO: true
            },
            end_date: {
            required: true,
            dateISO: true
            },
            start: {
            required: true
            },
            finish: {
            required: true
            },
            total_distance: {
            required: true,
            number: true
            }
        },
        messages: {
            start: {required: "This field is required."},
            start_date: {
            required: "This field is required.",
            dateISO: "Enter a valid date in YYYY-MM-DD format."
            },
            finish: {required: "This field is required."},
            end_date: {
            required: "This field is required.",
            dateISO: "Enter a valid date in YYYY-MM-DD format."
            },
            total_distance: {
            required: "This field is required.",
            number: "Please insert a valid distance."
            }
        },
        errorClass: "errorlist",
        onkeyup: false,
        onclick: false,
        onfocusout: false
    })
});